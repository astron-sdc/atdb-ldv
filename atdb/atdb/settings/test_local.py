from atdb.settings.base import *
import os

DATABASES = {
    'default': {
         'ENGINE': 'django.db.backends.postgresql_psycopg2',
         'USER': 'atdb_admin_test_local',
         'PASSWORD': 'atdb123_test_local',
         'NAME': 'atdb_ldv_test_local',
         'HOST': 'localhost',
         'PORT': '5555',
    },
}