
import os
import logging
import sys
sys.modules['fontawesome_free'] = __import__('fontawesome-free')

logger = logging.getLogger(__name__)

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# SECURITY WARNING: keep the secret key used in production secret!
try:
    SECRET_KEY = os.environ['SECRET_KEY']
except:
    SECRET_KEY = 'key_for_testing_only'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = ['*']
CORS_ORIGIN_ALLOW_ALL = True

# Application definition

INSTALLED_APPS = [
    'taskdatabase',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'rest_framework',
    'rest_framework.authtoken',
    'corsheaders',
    'django_filters',
    'django_extensions',
    'django_tables2',
    'bootstrap3',
    'fontawesome_free',
    #'silk',

    ## These are required for ASTRONauth
    'django.contrib.sites',
    "astronauth",  # it is important that astronauth is included before allauth
    'allauth',
    'allauth.account',
    'allauth.socialaccount',
    'allauth.socialaccount.providers.openid_connect',
]

MIDDLEWARE = [
    'corsheaders.middleware.CorsMiddleware',
    'django.middleware.security.SecurityMiddleware',
    'whitenoise.middleware.WhiteNoiseMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'allauth.account.middleware.AccountMiddleware',
    #'silk.middleware.SilkyMiddleware',
]

ROOT_URLCONF = 'atdb.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'django.template.context_processors.request',
            ],
        },
    },
]

WSGI_APPLICATION = 'atdb.wsgi.application'

REST_FRAMEWORK = {
    # Use Django's standard `django.contrib.auth` permissions,
    # or allow read-only access for unauthenticated users.
    'DEFAULT_PERMISSION_CLASSES': [
        'rest_framework.permissions.DjangoModelPermissionsOrAnonReadOnly'
    ],
    'DEFAULT_AUTHENTICATION_CLASSES': [
        'rest_framework.authentication.SessionAuthentication',
        'rest_framework.authentication.TokenAuthentication',
    ],
    'DEFAULT_FILTER_BACKENDS': (
        'django_filters.rest_framework.DjangoFilterBackend',
    ),
    'DEFAULT_PAGINATION_CLASS': 'rest_framework.pagination.PageNumberPagination',
    'PAGE_SIZE': 100
}

# Password validation
# https://docs.djangoproject.com/en/2.0/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = []


# Internationalization
# https://docs.djangoproject.com/en/2.0/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True

# Logging
# https://docs.djangoproject.com/en/1.11/topics/logging/#configuring-logging
# The default configuration: https://github.com/django/django/blob/stable/1.11.x/django/utils/log.py

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse',
        },
        'require_debug_true': {
            '()': 'django.utils.log.RequireDebugTrue',
        },
    },
    'formatters': {
        'my_formatter': {
            '()': 'django.utils.log.ServerFormatter',
            'format': '[%(asctime)s] %(message)s',
        }
    },
    'handlers': {
        'console': {
            'level': 'DEBUG',
            'filters': ['require_debug_true'],
            'class': 'logging.StreamHandler',
        },
        'django.server': {
            'level': 'INFO',
            'class': 'logging.StreamHandler',
            'formatter': 'my_formatter',
        },
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        },
        'my_handler': {
            'level': 'INFO',
            'class': 'logging.StreamHandler',
            'formatter': 'my_formatter',
        },

    },
    'loggers': {
        'taskdatabase': {
            #'handlers': ['my_handler','mail_admins'],
            'handlers': ['my_handler'],
            'level': 'INFO',
        },
        'django': {
            'handlers': ['console', 'mail_admins'],
            'level': 'INFO',
        },
        'django.server': {
            'handlers': ['django.server'],
            'level': 'INFO',
            'propagate': False,
        },
        'django.db.backends': {
            'handlers': ['console'],
            'level': 'ERROR',
        },
    }
}

from django.contrib.messages import constants as messages
MESSAGE_TAGS = {
        messages.DEBUG: 'alert-secondary',
        messages.INFO: 'alert-info',
        messages.SUCCESS: 'alert-success',
        messages.WARNING: 'alert-warning',
        messages.ERROR: 'alert-danger',
}

FORCE_SCRIPT_NAME = os.environ.get('ATDB_PATH', '/')
LOGIN_REDIRECT_URL = FORCE_SCRIPT_NAME + 'atdb'
LOGOUT_REDIRECT_URL = FORCE_SCRIPT_NAME + 'atdb'

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/2.0/howto/static-files/

#STATIC_URL = '/static_atdb/'
STATIC_URL = FORCE_SCRIPT_NAME + 'atdb/static/'
STATIC_ROOT = os.path.join(BASE_DIR, 'static')

try:
    MEDIA_ROOT = os.environ['MEDIA_ROOT']
except:
    MEDIA_ROOT = os.path.join(BASE_DIR, 'media')

MEDIA_URL = FORCE_SCRIPT_NAME + 'atdb/media/'

ALL_STATUSSES = ['defining','defined','staging','staged','processing','processed','storing','stored','scrubbing','scrubbed','validated', 'pre_archiving','pre_archived', 'archiving','archived','finishing','finished']
ACTIVE_STATUSSES = ['staging','staged','processing','processed','validated','storing','stored','scrubbing','scrubbed','pre_archiving','pre_archived', 'archiving','archived']
STATUSSES_WITH_DATA = ['staged','fetching','fetched','processing','processed','validated','storing','stored','scrubbing','scrubbed','pre_archiving','pre_archived', 'archiving','archived']
AGGREGATES = ['failed','active','total']

QUERY_LIMIT_MULTI_CHANGE = 10000
MAX_MONITORING_HISTORY_HOURS = 7 * 24
SERVICES_LATE_WARNING_SECONDS = 1800

# astronauth settings
SITE_ID = 1

#ACCOUNT_DEFAULT_HTTP_PROTOCOL = 'https'
USE_X_FORWARDED_HOST = True
SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')

AUTHENTICATION_BACKENDS = [
    # Needed to login by username in Django admin, regardless of `allauth`
    "django.contrib.auth.backends.ModelBackend",
    # `allauth` specific authentication methods, such as login by e-mail
    "allauth.account.auth_backends.AuthenticationBackend",
]

# Set your keycloak url and realm
SOCIALACCOUNT_PROVIDERS = {
    "openid_connect": {
        "SERVERS": [
            {
                "id": "keycloak",
                "name": "Keycloak",
                "server_url": os.getenv(
                    "KEYCLOAK_URL", "https://keycloak.astron.nl/auth"
                )
                + "/realms/"
                + os.getenv("KEYCLOAK_REALM", "SDC")
                + "/.well-known/openid-configuration",
                "APP": {
                    "client_id": os.getenv("KEYCLOAK_CLIENT_ID"),
                    "secret": os.getenv("KEYCLOAK_CLIENT_SECRET"),
                },
                "SCOPE": ["openid", "profile", "email"],
            }
        ]
    },
}

# Disable signing up for local accounts
SOCIALACCOUNT_ONLY=True
ACCOUNT_EMAIL_VERIFICATION='none'


try:
    LOGIN_REDIRECT_URL = os.environ['LOGIN_REDIRECT_URL']
except:
    LOGIN_REDIRECT_URL = '/atdb/'

logger.info("LOGIN_REDIRECT_URL:" + LOGIN_REDIRECT_URL)


SESSION_COOKIE_NAME = 'atdb_session_id'
CSRF_COOKIE_NAME = 'atdb_csrftoken'

#SILKY_PYTHON_PROFILER = False
#SILKY_PYTHON_PROFILER_BINARY = False

DEFAULT_AUTO_FIELD = 'django.db.models.AutoField'
ACCOUNT_UNIQUE_EMAIL = False

# this limits the size of the 'monitoring' table where all the running services report heartbeats to ATDB
MAX_MONITORING_HISTORY_HOURS = 168  # 1 week