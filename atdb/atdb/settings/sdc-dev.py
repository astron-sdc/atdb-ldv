from atdb.settings.base import *
import os

# SECURITY WARNING: don't run with debug turned on in production!
DEV = True
DEBUG = True

ALLOWED_HOSTS = ["*"]
CORS_ORIGIN_ALLOW_ALL = True

# needs to be inside the ASTRON network or connected through VPN
DATABASES = {
    'default': {
         'ENGINE': 'django.db.backends.postgresql_psycopg2',
         'USER': 'dbadmin',
         'PASSWORD': 'dbadmin123',
         'NAME': 'atdbldv',
         'HOST': 'sdc-dev.astron.nl',
         'PORT': '10000',
    },
}

# Password validation
# https://docs.djangoproject.com/en/1.10/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = []

LOGIN_REDIRECT_URL = "http://localhost:8000/atdb"
