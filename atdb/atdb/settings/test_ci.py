from atdb.settings.base import *
import os

DATABASES = {
    'default': {
         'ENGINE': 'django.db.backends.postgresql_psycopg2',
         'USER': 'atdb_admin_test_ci',
         'PASSWORD': 'atdb123_test_ci',
         'NAME': 'atdb_ldv_test_ci',
         'HOST': 'postgres',
         'PORT': '5432',
    },
}