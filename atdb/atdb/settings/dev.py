from atdb.settings.base import *
import os

# SECURITY WARNING: don't run with debug turned on in production!
DEV = True
DEBUG = True

ALLOWED_HOSTS = ["*"]
CORS_ORIGIN_ALLOW_ALL = True

DATABASES = {
    'default': {
         'ENGINE': 'django.db.backends.postgresql_psycopg2',
         'USER': 'atdb_admin',
         'PASSWORD': 'atdb123',
         'NAME': 'atdb_ldv_1mar2024',
         #'NAME': 'atdb_ldv_17feb2024',
         'HOST': 'localhost',
         'PORT': '5432',
    },
}

# Password validation
# https://docs.djangoproject.com/en/1.10/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = []

LOGIN_REDIRECT_URL = "http://localhost:8000/atdb"

#MEDIA_ROOT = 'shared/media/'