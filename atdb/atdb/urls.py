
from django.contrib import admin
from django.urls import include, path
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('atdb/', include('taskdatabase.urls')),
    path('atdb/admin/', admin.site.urls),
    path('atdb/api-auth/', include('rest_framework.urls')),
    path("atdb/astronauth/", include("astronauth.urls")),  # include astronauth
    #path('atdb/silk/', include('silk.urls', namespace='silk'))
]

#if settings.DEBUG:
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
