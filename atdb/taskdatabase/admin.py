from django.contrib import admin
from .models import Status, Task, Activity, Workflow, LogEntry, Configuration, Job, PostProcessingRule, Monitor, LatestMonitor

admin.site.register(Status)

@admin.register(Task)
class TaskAdmin(admin.ModelAdmin):
    ordering = ['-creationTime']
    search_fields = ['id','sas_id']

@admin.register(Activity)
class ActivityAdmin(admin.ModelAdmin):
    ordering = ['-sas_id']
    search_fields = ['id','sas_id']

admin.site.register(Workflow)
admin.site.register(LogEntry)
admin.site.register(Configuration)
admin.site.register(Job)
admin.site.register(PostProcessingRule)
admin.site.register(Monitor)
admin.site.register(LatestMonitor)