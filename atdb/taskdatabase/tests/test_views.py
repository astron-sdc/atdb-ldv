from django.test import TestCase, RequestFactory
from django.urls import reverse

from django.contrib.auth.models import User
from django.contrib.sessions.middleware import SessionMiddleware
from django.contrib.messages.storage.fallback import FallbackStorage

from taskdatabase.models import Task, Workflow, LatestMonitor
from taskdatabase.views import SortTasks, TaskMultiStatus, TaskMultiHold, TaskMultiPurge, Hold, HoldQuery, PurgeQuery, \
    TaskSetStatus, TaskRetry, TaskDiscard, TaskDiscardSasId, ServiceHoldResume, TaskValidateTask,TaskValidateSasId,\
    convert_query_params_to_url, GetSizeView, ShowTaskQuality, AnnotateQualitySasId, AnnotateQualityTaskId, \
    ClearAnnotationsSasID

from django.db.models.query import QuerySet
from unittest.mock import patch, MagicMock

class TestViews(TestCase):
    def setUp(self):
        self.factory = RequestFactory()

        # Create a test workflow
        workflow_requantisation = Workflow(workflow_uri="psrfits_requantisation")
        workflow_requantisation.save()

        # Create a test task
        self.task1 = Task.objects.create(sas_id=123, status='defined', workflow=workflow_requantisation,
                                         remarks = {"discard_reason" : "no longer needed",
                                                    "quality_sasid" : "initial remark for sas_id",
                                                    "quality_taskid" : "initial remark for task_id"})
        self.task2 = Task.objects.create(sas_id=456, status='defined', workflow=workflow_requantisation)
        self.task3 = Task.objects.create(sas_id=789, status='processed', workflow=workflow_requantisation)
        self.task4 = Task.objects.create(sas_id=123, status='stored', workflow=workflow_requantisation,
                                         remarks = {"discard_reason" : "no longer needed"})
        self.task5 = Task.objects.create(sas_id=555, status='stored', workflow=workflow_requantisation,
                                         calculated_qualities = {"per_task" : "moderate","per_sasid" : "good"})
        self.task6 = Task.objects.create(sas_id=555, status='stored', workflow=workflow_requantisation,
                                         calculated_qualities = {"per_sasid" : "good"})

        self.user = User.objects.create_user(username='testuser', password='testpass')

        self.session_data = {
            'filtered_tasks_as_list': [self.task1.id, self.task2.id],
            'current_query_params': 'status=defined'
        }

        self.aggregator_service = LatestMonitor.objects.create(name="aggregator",hostname="my_host",metadata={"enabled":"True"})


    def _set_up_session(self, request):
        """Helper function to set up session for the request"""
        middleware = SessionMiddleware(get_response=lambda r: None)
        middleware.process_request(request)
        request.session.update(self.session_data)
        request.session.save()

        # Add messages framework to the request
        request._messages = FallbackStorage(request)

    def test_basic_conversion(self):
        query_params = "<QueryDict: {'filter': ['nv_123'], 'status': ['stored'], 'sas_id': ['12345']}>"
        expected_result = "&filter=nv_123&status=stored&sas_id=12345"
        self.assertEqual(convert_query_params_to_url(query_params), expected_result)

    def test_sort(self):
        """
        test SortTasks
        """
        # Set up the URL for the view

        # Arrange
        # Create a request object
        request = self.factory.get('/dummy-url')
        self._set_up_session(request)

        # Act
        # Call the function with sort='priority' and redirect_to_page='tasks_list'
        response = SortTasks(request, sort='sas_id', redirect_to_page='atdb')

        # Assert
        # Check if the sort field is correctly stored in the session
        self.assertEqual(request.session['sort'], 'sas_id')

        # Check if it redirects to the 'index' page
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.url, reverse('index'))


    def test_task_set_status(self):
        """
        test TaskSetStatus
        """

        # Arrange
        request = self.factory.post('/dummy-url')
        self._set_up_session(request)
        request.user = self.user

        # Act
        response = TaskSetStatus(request, self.task3.id, new_status='finished', page = 0)
        self.task3.refresh_from_db()
        # Assert
        # Check that the tasks were updated correctly
        self.assertEqual(self.task3.status,'finished')

        # check redirection
        self.assertEqual(response.status_code, 302)

    def test_task_retry(self):
        """
        test TaskRetry
        """

        # Arrange
        request = self.factory.post('/dummy-url')
        self._set_up_session(request)
        request.user = self.user

        # Act
        response = TaskRetry(request, self.task3.id, new_status='processed', page = 0)

        # Assert
        # Check that the tasks were updated correctly
        self.assertEqual(self.task3.status,'processed')

        # check redirection
        self.assertEqual(response.status_code, 302)


    @patch('taskdatabase.views.Task.objects.get')
    @patch('taskdatabase.views.DiscardAnnotationForm.is_valid')
    def test_task_multi_status_post(self, mock_is_valid, mock_get):
        """
        test TaskMultiStatus (post)
        """

        # Arrange
        # Mock the Task.objects.get method to return mock tasks
        mock_get.side_effect = lambda id: self.task1 if id == self.task1.id else self.task2
        mock_is_valid.return_value = True

        request = self.factory.post('/dummy-url', data={'annotation': 'test annotation'})
        self._set_up_session(request)
        request.user = self.user

        # Act
        # Call the function with new_status='discarded'
        response = TaskMultiStatus(request, new_status='discarded', query_params='status=defined')

        # Assert
        # Check that the tasks were updated correctly
        self.assertEqual(self.task1.status, 'discarded')
        self.assertEqual(self.task2.status, 'discarded')
        self.assertEqual(self.task1.remarks['discard_reason'], 'test annotation')
        self.assertEqual(self.task2.remarks['discard_reason'], 'test annotation')

        # Check if it redirects to the correct URL
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.url, reverse('query') + '?' + 'status=defined')

    @patch('taskdatabase.views.convert_query_params_to_url')
    def test_task_multi_status_get(self,mock_query_params):
        """
        test TaskMultiStatus (get)
        """

        # Arrange
        mock_query_params.return_value = "&status=defined"

        request = self.factory.get('/dummy-url')
        self._set_up_session(request)
        request.user = self.user
        expected_params_on_session = "&status=defined"

        # Act
        # Call the function with new_status='discarded'
        response = TaskMultiStatus(request, new_status='discarded', query_params='status=defined')

        # Assert
        self.assertEqual(response.status_code, 200)
        self.assertEqual(request.session['current_query_params'], expected_params_on_session)

    def test_task_hold(self):
        """
        test Hold
        """

        # Arrange
        request = self.factory.post('/dummy-url')
        self._set_up_session(request)
        request.user = self.user

        # Act
        response = Hold(request, self.task1.id, hold_it='resume')

        # Assert
        # Check that the tasks were updated correctly
        self.assertTrue(self.task1.resume)

        # Check if it redirects to the correct URL
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.url, reverse('query'))

    @patch('taskdatabase.views.convert_query_params_to_url')
    def test_task_hold_query(self, mock_query_params):
        """
        test HoldQuery
        """

        # Arrange
        mock_query_params.return_value = "&status=defined"
        request = self.factory.post('/dummy-url')
        self._set_up_session(request)
        request.user = self.user

        # Act
        response = HoldQuery(request, self.task1.id, hold_it='resume', query_params='status=defined')

        # Assert
        # Check that the tasks were updated correctly
        self.assertTrue(self.task1.resume)

        # check redirection
        self.assertEqual(response.status_code, 302)

    @patch('taskdatabase.views.Task.objects.get')
    def test_task_multi_hold_post(self, mock_get):
        """
        test TaskMultiHold
        """

        # Arrange
        # Mock the Task.objects.get method to return mock tasks
        mock_get.side_effect = lambda id: self.task1 if id == self.task1.id else self.task2

        request = self.factory.post('/dummy-url')
        self._set_up_session(request)
        request.user = self.user

        # Act
        response = TaskMultiHold(request, onhold='resume', query_params='status=defined')

        # Assert
        # Check that the tasks were updated correctly
        self.assertTrue(self.task1.resume)
        self.assertTrue(self.task2.resume)
        self.assertEqual(response.status_code, 302)


    @patch('taskdatabase.views.convert_query_params_to_url')
    def test_task_purge_query(self, mock_query_params):
        """
        test PurgeQuery
        """

        # Arrange
        mock_query_params.return_value = "&status=defined"
        request = self.factory.post('/dummy-url')
        self._set_up_session(request)
        request.user = self.user

        # Act
        response = PurgeQuery(request, self.task1.id, purge_policy='no', query_params='status=defined')

        # Assert
        # Check that the tasks were updated correctly
        self.assertEqual(self.task1.purge_policy,"no")
        # check redirection
        self.assertEqual(response.status_code, 302)

    @patch('taskdatabase.views.Task.objects.get')
    def test_task_multi_purge_post(self, mock_get):
        """
        test TaskMultiPurge
        """

        # Arrange
        # Mock the Task.objects.get method to return mock tasks
        mock_get.side_effect = lambda id: self.task1 if id == self.task1.id else self.task2

        request = self.factory.post('/dummy-url')
        self._set_up_session(request)
        request.user = self.user

        # Act
        response = TaskMultiPurge(request, purge_policy='yes', query_params='status=defined')

        # Assert
        # Check that the tasks were updated correctly
        self.assertEqual(self.task1.purge_policy,"yes")
        self.assertEqual(self.task2.purge_policy,"yes")
        self.assertEqual(response.status_code, 302)


    @patch('taskdatabase.views.Task.objects.get')
    @patch('taskdatabase.views.DiscardAnnotationForm.is_valid')
    def test_task_discard_post(self, mock_is_valid, mock_get):
        """
        test TaskDiscard
        """

        # Arrange
        # Mock the Task.objects.get method to return mock tasks
        mock_get.return_value = self.task1
        mock_is_valid.return_value = True

        request = self.factory.post('/dummy-url', data={'annotation': 'test annotation'})
        self._set_up_session(request)
        request.user = self.user

        # Act
        # Call the function with new_status='discarded'
        response = TaskDiscard(request, self.task1.id, new_status='discard', page=0)

        # Assert
        # Check that the tasks were updated correctly
        self.assertEqual(self.task1.status, 'discard')
        self.assertEqual(self.task1.remarks['discard_reason'], 'test annotation')

        # Check if it redirects to the correct URL
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.url, reverse('task-details'))

    @patch('taskdatabase.views.Task.objects.get')
    def test_task_discard_get(self, mock_get):
        """
        test TaskDiscard
        """

        # Arrange
        mock_get.return_value = self.task1
        request = self.factory.get('/dummy-url')
        self._set_up_session(request)
        request.user = self.user

        # Act
        # Call the function with new_status='discarded'
        response = TaskDiscard(request, self.task1.id, new_status='discard', page=0)

        # Assert
        self.assertEqual(response.status_code, 200)
        self.assertEqual(self.task1.remarks['discard_reason'], 'no longer needed')


    @patch('taskdatabase.views.Task.objects.get')
    @patch('taskdatabase.views.Task.objects.filter')
    @patch('taskdatabase.views.DiscardAnnotationForm.is_valid')
    def test_discard_sasid_post(self, mock_is_valid, mock_filter, mock_get):
        """
        test TaskDiscardSasId
        """

        # Arrange
        # Mock the Task.objects.get method to return mock tasks
        mock_get.return_value = self.task1
        mock_filter.return_value = [self.task1,self.task4]
        mock_is_valid.return_value = True

        request = self.factory.post('/dummy-url')
        self._set_up_session(request)
        request.user = self.user

        # Act
        response = TaskDiscardSasId(request, self.task1.id, new_status='discard', page="0")

        # Assert
        self.assertEqual(self.task1.status, 'discard')
        self.assertEqual(response.status_code, 302)

    @patch('taskdatabase.views.Task.objects.get')
    @patch('taskdatabase.views.Task.objects.filter')
    def test_discard_sasid_get(self, mock_filter, mock_get):
        """
        test TaskDiscardSasId
        """

        # Arrange
        # Mock the Task.objects.get method to return mock tasks
        mock_get.return_value = self.task1

        # mock the queryset, because tasks.count() is used in TaskDiscardSasId
        mock_queryset = MagicMock(spec=QuerySet)
        mock_filter.return_value = mock_queryset
        mock_queryset.count.return_value = 2
        mock_queryset.__iter__.return_value = iter([self.task1, self.task4])

        request = self.factory.get('/dummy-url')
        self._set_up_session(request)
        request.user = self.user

        # Act
        response = TaskDiscardSasId(request, self.task1.id, new_status='discard', page="0")

        # Assert
        self.assertEqual(response.status_code, 200)
        self.assertEqual(self.task1.remarks['discard_reason'], 'no longer needed')




    @patch('taskdatabase.views.Task.objects.get')
    def test_validate_task(self, mock_get):
        """
        test TaskValidateTask
        """

        # Arrange
        mock_get.return_value = self.task5

        request = self.factory.get('/dummy-url')
        self._set_up_session(request)
        request.user = self.user

        # Act
        response = TaskValidateTask(request, self.task5.id, quality='calculated', new_status="validated", page="0")

        # Assert
        self.assertEqual(self.task5.quality, 'moderate')
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.url, reverse('quality') + '?' + 'page=0')


    @patch('taskdatabase.views.algorithms.get_size')  # Mock the get_size function
    def test_get_size_view(self, mock_get_size):
        # Setup
        mock_get_size.return_value = 100

        request = self.factory.get('/get-size/?status__in=processed,stored&type=to_process')
        view = GetSizeView.as_view()

        # Act
        response = view(request)

        # Assert
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data, {'total_size': 100})
        mock_get_size.assert_called_once_with(['processed', 'stored'], 'to_process')


    @patch('taskdatabase.views.algorithms.convert_quality_to_html')
    @patch('taskdatabase.views.algorithms.construct_inspectionplots')
    def test_show_task_quality_with_valid_id(self, mock_construct_inspectionplots, mock_convert_quality_to_html):
        """
        test ShowTaskQuality
        """

        request = self.factory.get('/task_quality/')
        self._set_up_session(request)

        # Mocking the algorithms
        mock_convert_quality_to_html.return_value = "<p>Quality HTML</p>"
        mock_construct_inspectionplots.return_value = "<p>Plots HTML</p>"

        response = ShowTaskQuality(request, id=self.task1.id)

        # Asserts
        self.assertEqual(response.status_code, 200)
        self.assertIn('Quality HTML', response.content.decode())
        self.assertIn('Plots HTML', response.content.decode())
        self.assertEqual(request.session['task_id'], self.task1.id)

    def test_show_task_quality_with_invalid_id_no_task_in_session(self):
        """
        test ShowTaskQuality
        """
        request = self.factory.get('/task_quality/')
        self._set_up_session(request)

        response = ShowTaskQuality(request, id=0)

        # Asserts
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.url, '/atdb/')  # Assuming 'index' redirects to '/'
        self.assertEqual(len(request._messages), 1)


    @patch('taskdatabase.views.Task.objects.get')
    @patch('taskdatabase.views.Task.objects.filter')
    @patch('taskdatabase.views.QualityAnnotationForm.is_valid')
    def test_annotate_quality_sasid_post(self, mock_is_valid, mock_filter, mock_get):
        """
        test AnnotateQualitySasID (post)
        """

        # Arrange
        # Mock the Task.objects.get method to return mock tasks
        mock_get.return_value = self.task1
        mock_filter.return_value = [self.task1,self.task4]
        mock_is_valid.return_value = True

        post_data = {'annotation': 'quality annotation','return_to_page' : 0}
        request = self.factory.post('/dummy-url', data=post_data)
        self._set_up_session(request)

        # Act
        response = AnnotateQualitySasId(request, self.task1.id)
        self.task1.refresh_from_db()

        # Assert
        self.assertEqual(self.task1.remarks['quality_sasid'], 'quality annotation')
        self.assertEqual(response.status_code, 302)

    @patch('taskdatabase.views.Task.objects.get')
    def test_annotate_quality_sasid_get(self, mock_get):
        """
        test AnnotateQualitySasID (get)
        """

        # Arrange
        # Mock the Task.objects.get method to return mock tasks
        mock_get.return_value = self.task1

        request = self.factory.get('/dummy-url')
        self._set_up_session(request)
        request.user = self.user

        # Act
        response = AnnotateQualitySasId(request, self.task1.id)

        # Assert
        self.assertEqual(response.status_code, 200)
        self.assertEqual(self.task1.remarks['quality_sasid'], 'initial remark for sas_id')

    @patch('taskdatabase.views.Task.objects.get')
    @patch('taskdatabase.views.QualityAnnotationForm.is_valid')
    def test_annotate_quality_taskid_post(self, mock_is_valid, mock_get):
        """
        test AnnotateQualityTaskId (post)
        """

        # Arrange
        # Mock the Task.objects.get method to return mock tasks
        mock_get.return_value = self.task1
        mock_is_valid.return_value = True

        post_data = {'annotation': 'quality annotation','return_to_page' : 0}
        request = self.factory.post('/dummy-url', data=post_data)
        self._set_up_session(request)

        # Act
        response = AnnotateQualityTaskId(request, self.task1.id)
        self.task1.refresh_from_db()

        # Assert
        self.assertEqual(self.task1.remarks['quality_taskid'], 'quality annotation')
        self.assertEqual(response.status_code, 302)

    @patch('taskdatabase.views.Task.objects.get')
    def test_annotate_quality_taskid_get(self, mock_get):
        """
        test AnnotateQualityTaskId (get)
        """

        # Arrange
        # Mock the Task.objects.get method to return mock tasks
        mock_get.return_value = self.task1

        request = self.factory.get('/dummy-url')
        self._set_up_session(request)
        request.user = self.user

        # Act
        response = AnnotateQualityTaskId(request, self.task1.id)

        # Assert
        self.assertEqual(response.status_code, 200)
        self.assertEqual(self.task1.remarks['quality_taskid'], 'initial remark for task_id')

    @patch('taskdatabase.models.Task.objects.get')
    @patch('taskdatabase.models.Task.objects.filter')
    def test_clear_annotations_sas_id(self, mock_filter, mock_get):
        # Arrange
        # Mock the queryset returned by Task.objects.filter
        mock_get.return_value = self.task1
        mock_filter.return_value = [self.task1, self.task2]

        request = MagicMock()

        # Act
        response = ClearAnnotationsSasID(request, id=self.task1.id)

        # Asserts
        self.assertEqual(response.status_code, 302)  # Redirect status code
        self.assertEqual(response.url, reverse('validation'))

        # Verify that the remarks were cleared
        self.assertIsNone(self.task1.remarks['quality_sasid'])
        self.assertIsNone(self.task2.remarks['quality_sasid'])
