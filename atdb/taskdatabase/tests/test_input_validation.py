from django.test import TestCase
from django.utils import timezone
from datetime import datetime
from taskdatabase.models import LogEntry, Task, Workflow
from taskdatabase.services.specification import input_validation

class TestInputValidation(TestCase):

    def setUp(self):

        # used to test the get_size calculation, this uses a database
        self.workflow = Workflow(id=22, workflow_uri="psrfits_requantisation")
        self.workflow.save()

        self.inputs_with_duplicate = [
        {
            "size": 100,
            "surl": "srm://srm.grid.sara.nl:8443/pnfs/grid.sara.nl/data/lofar/ops/projects/lc5_020/432340/L432340_SB000_uv.dppp.MS_caf35c3d.tar",
            "type": "File",
            "location": "srm.grid.sara.nl"
        },
        {
            "size": 200,
            "surl": "srm://srm.grid.sara.nl:8443/pnfs/grid.sara.nl/data/lofar/ops/projects/lc5_020/432340/L432340_SB001_uv.dppp.MS_74948c4c.tar",
            "type": "File",
            "location": "srm.grid.sara.nl"
        },
        {
            "size": 300,
            "surl": "srm://srm.grid.sara.nl:8443/pnfs/grid.sara.nl/data/lofar/ops/projects/lc5_020/432340/L432340_SB000_uv.dppp.MS_caf35c3d.tar",
            "type": "File",
            "location": "srm.grid.sara.nl"
        }]

        self.inputs_validated = [
        {
            "size": 100,
            "surl": "srm://srm.grid.sara.nl:8443/pnfs/grid.sara.nl/data/lofar/ops/projects/lc5_020/432340/L432340_SB000_uv.dppp.MS_caf35c3d.tar",
            "type": "File",
            "location": "srm.grid.sara.nl"
        },
        {
            "size": 200,
            "surl": "srm://srm.grid.sara.nl:8443/pnfs/grid.sara.nl/data/lofar/ops/projects/lc5_020/432340/L432340_SB001_uv.dppp.MS_74948c4c.tar",
            "type": "File",
            "location": "srm.grid.sara.nl"
        }]

        self.task = Task.objects.create(sas_id='5432', status='defining', new_status='defining',
                                        size_to_process = 100, workflow=self.workflow, inputs = self.inputs_with_duplicate)
        self.task.save()


    def test_check_duplicates(self):
        """
        test if duplicates are detected
        """
        # arrange
        expected_size = 600
        expected_duplicates = ['L432340_SB000_uv.dppp.MS.tar']

        # act
        input_validation.validate_inputs(self.task)
        found_duplicates, duplicates, unique_inputs = input_validation.check_duplicates(self.inputs_with_duplicate)

        # assert
        self.assertEqual(duplicates, expected_duplicates)
        self.assertEqual(self.task.size_to_process, expected_size)


    def test_trigger_on_defined(self):
        """
        test that the functionality (also) triggers on 'defined'
        """

        # arrange
        expected_size = 300

        # set to 'wrong' inputs containing a duplicate
        self.task.inputs = self.inputs_validated

        # act
        self.task.new_status="defined"
        self.task.save()

        # assert
        self.assertEqual(self.task.size_to_process, expected_size)

    def test_no_trigger_on_staged(self):
        """
        test that the functionality does not trigger on other statusses, like 'staged'
        the inputs.validated has a total sum of 300, but 'staged' should not recalculate the new size.
        """

        # arrange
        expected_size = 600

        # set to 'wrong' inputs containing a duplicate
        self.task.inputs = self.inputs_validated

        # act
        self.task.new_status="staged"
        self.task.save()

        # assert
        self.assertEqual(self.task.size_to_process, expected_size)
