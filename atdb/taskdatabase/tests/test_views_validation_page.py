from django.test import TestCase
from django.urls import reverse

from taskdatabase.models import Task, Workflow
class ValidationPageViewTest(TestCase):

    @classmethod
    def setUpTestData(cls):

        # Set up non-modified objects used by all test methods
        workflow = Workflow()
        workflow.save()

        # create a list of Tasks
        Task.objects.get_or_create(sas_id=12345, status='stored', workflow = workflow)
        Task.objects.get_or_create(sas_id=12345, status='stored', workflow = workflow)
        Task.objects.get_or_create(sas_id=12345, status='stored', workflow = workflow)

    def test_url_exists_at_desired_location(self):
        response = self.client.get('/atdb/validation')
        self.assertEqual(response.status_code, 200)

    def test_url_accessible_by_name(self):
        response = self.client.get(reverse('validation'))
        self.assertEqual(response.status_code, 200)

    def test_uses_correct_template(self):
        response = self.client.get(reverse('validation'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'taskdatabase/validation/page.html')