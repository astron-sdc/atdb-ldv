from django.test import TestCase
from django.test import RequestFactory
from django.db.models.query import QuerySet
from django.contrib.sessions.middleware import SessionMiddleware
from unittest.mock import patch, MagicMock

from taskdatabase.models import Task, Workflow, Activity
from taskdatabase.views import get_filtered_tasks

class TestFilters(TestCase):
    def setUp(self):
        self.workflow_requantisation = Workflow(id=22, workflow_uri="psrfits_requantisation")
        self.workflow_requantisation.save()
        self.activity_12345 = Activity.objects.create(sas_id=12345)
        self.activity_12345.save()
        self.activity_66666 = Activity.objects.create(sas_id=66666)
        self.activity_66666.save()
        self.task1 = Task.objects.create(sas_id=12345, status='defined', workflow=self.workflow_requantisation, activity = self.activity_12345)
        self.task2 = Task.objects.create(sas_id=12345, status='scrubbed', workflow=self.workflow_requantisation, activity = self.activity_12345)
        self.task3 = Task.objects.create(sas_id=12345, status='scrubbed', workflow=self.workflow_requantisation, activity = self.activity_12345)
        self.task4 = Task.objects.create(sas_id=66666, status='scrubbed', workflow=self.workflow_requantisation, activity = self.activity_66666)
        self.task5 = Task.objects.create(sas_id=66666, status='archived_failed', workflow=self.workflow_requantisation, activity = self.activity_66666)

    def _set_up_session(self, request):
        """Helper function to set up session for the request"""
        middleware = SessionMiddleware(get_response=lambda r: None)
        middleware.process_request(request)
        #request.session.update(self.session_data)
        request.session.save()

    def test_without_filter(self):
        count = 0
        for task in Task.objects.all():
            count += 1

        self.assertEqual(count,5)

    def test_with_ingest_filter(self):
        # Arrange
        # create a request object
        request = RequestFactory().get('/atdb/ingest')

        # this simulates the 'Queue (scrubbed)' filter on the ingest page
        request.session = {'ingest_filter': 'scrubbed'}

        # Act
        # after aggregating per sas_id, 2 objects with status 'scrubbed' remain
        tasks = get_filtered_tasks(request, None, "sas_id")

        # Assert
        self.assertEqual(tasks.count(), 2)


    def test_with_task_status_filter(self):
        # Arrange
        # create a request object
        request = RequestFactory().get('/atdb')
        request.session = {'task_filter': 'scrubbed'}

        # Act
        tasks = get_filtered_tasks(request, None, None)

        # Assert
        self.assertEqual(tasks.count(), 3)

    def test_with_filtered_task_on_session(self):
        """
        test with pre_filtered tasks on the session
        """

        # Arrange
        # create a request object
        request = RequestFactory().get('/atdb')

        filtered_tasks_as_list = [self.task1.id,self.task2.id,self.task3.id]
        request.session = {'filtered_tasks_as_list': filtered_tasks_as_list}

        # Act
        tasks = get_filtered_tasks(request, None, None)

        # Assert
        self.assertEqual(tasks.count(), 3)


    def test_with_prefiltered_task_as_parameter(self):
        """
        test with pre_filtered tasks as function parameter
        """

        # Arrange
        # create a request object
        request = RequestFactory().get('/atdb')
        self._set_up_session(request)
        pre_filtered_tasks = Task.objects.all()

        # Act
        # after aggregating per sas_id, 2 objects with status 'scrubbed' remain
        tasks = get_filtered_tasks(request, pre_filtered_tasks, None)

        # Assert
        self.assertEqual(tasks.count(), 5)

