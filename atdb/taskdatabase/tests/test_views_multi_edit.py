from django.test import TestCase, RequestFactory
from django.urls import reverse
from django.contrib.sessions.middleware import SessionMiddleware
from django.contrib.messages.middleware import MessageMiddleware
from django.contrib.messages import get_messages
from taskdatabase.models import Task, Workflow
from taskdatabase.views import TaskMultiEdit

class TaskMultiEditTests(TestCase):

    def setUp(self):
        # Create some sample tasks
        self.workflow_requantisation = Workflow(id=22, workflow_uri="psrfits_requantisation")
        self.workflow_requantisation.save()
        self.task1 = Task.objects.create(sas_id=12345,filter="foo",status='stored',
                                         workflow=self.workflow_requantisation)
        self.task2 = Task.objects.create(sas_id=12345, filter="bar", status='stored',
                                         workflow=self.workflow_requantisation)
        self.factory = RequestFactory()

        self.query_params = "<QueryDict: {'id': [''], 'id__gte': [''], 'id__lte': [''], 'task_type__icontains': [''], 'task_type__in': [''], 'workflow__id': [''], 'filter': [''], 'filter__icontains': [''], 'priority': [''], 'priority__gte': [''], 'priority__lte': [''], 'status__icontains': [''], 'status__in': [''], 'quality__icontains': [''], 'quality__in': [''], 'project': [''], 'project__icontains': [''], 'project__in': [''], 'sas_id': ['12345'], 'sas_id__icontains': [''], 'sas_id__in': [''], 'purge_policy__icontains': [''], 'service_filter__icontains': ['']}>"

    def add_middleware(self, request):
        # Add session middleware
        session_middleware = SessionMiddleware(lambda req: None)
        session_middleware.process_request(request)
        request.session.save()

        # Add message middleware
        message_middleware = MessageMiddleware(lambda req: None)
        message_middleware.process_request(request)
        return request

    def test_valid_tasks_get_request(self):
        """Test GET request with valid task IDs."""
        request = self.factory.get('/task-multi-edit')
        request = self.add_middleware(request)
        request.session['filtered_tasks_as_list'] = [self.task1.id, self.task2.id]

        response = TaskMultiEdit(request, self.query_params)
        self.assertEqual(response.status_code, 200)


    def test_post_request_valid_form(self):
        """Test POST request with valid form data."""
        request = self.factory.post('/tasks/multi_edit/', {
            'sas_id_update': True,
            'sas_id': '54321',
        })
        request = self.add_middleware(request)
        request.session['filtered_tasks_as_list'] = [self.task1.id, self.task2.id]

        response = TaskMultiEdit(request, self.query_params)
        self.assertEqual(response.status_code, 200)  # Redirect after successful POST
        self.task1.refresh_from_db()
        self.task2.refresh_from_db()
        self.assertEqual(self.task1.sas_id, '12345')
        self.assertEqual(self.task2.sas_id, '12345')

    def test_post_request_no_fields_selected(self):
        """Test POST request when no fields are selected for update."""
        request = self.factory.post('/tasks/multi_edit/', {})
        request = self.add_middleware(request)
        request.session['filtered_tasks_as_list'] = [self.task1.id, self.task2.id]

        response = TaskMultiEdit(request, self.query_params)
        self.assertEqual(response.status_code, 200)
        messages = list(get_messages(request))
        self.assertEqual(len(messages), 0)

