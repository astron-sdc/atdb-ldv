from django.test import TestCase, RequestFactory
from django.urls import reverse

from django.contrib.auth.models import User
from django.contrib.sessions.middleware import SessionMiddleware

from taskdatabase.models import LatestMonitor
from taskdatabase.views import ServiceHoldResume

from unittest.mock import patch

class TestServices(TestCase):
    def setUp(self):
        self.factory = RequestFactory()

        self.user = User.objects.create_user(username='testuser', password='testpass')
        self.aggregator_service = LatestMonitor.objects.create(name="aggregator",hostname="my_host",metadata={"enabled":"True"})

    def _set_up_session(self, request):
        """Helper function to set up session for the request"""
        middleware = SessionMiddleware(get_response=lambda r: None)
        middleware.process_request(request)
        request.session.save()

    @patch('taskdatabase.views.LatestMonitor.objects.get')
    def test_service_hold_resume(self, mock_get):
        """
        test ServiceHoldResume
        """

        # Arrange
        mock_get.return_value = self.aggregator_service

        request = self.factory.get('/dummy-url')
        self._set_up_session(request)
        request.user = self.user

        # Act
        response = ServiceHoldResume(request, "aggregator", "my_host", "False")
        self.aggregator_service.refresh_from_db()

        # Assert
        enabled = self.aggregator_service.metadata['enabled']
        self.assertEqual(enabled, "False")

        # Check if it redirects to the correct URL
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.url, reverse('monitoring'))