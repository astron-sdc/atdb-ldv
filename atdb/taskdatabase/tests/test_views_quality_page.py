from django.test import TestCase
from django.urls import reverse
from django.test import RequestFactory
from django.contrib.sessions.middleware import SessionMiddleware
from django.core.paginator import Paginator

from taskdatabase.models import Task, Workflow
from taskdatabase.views import ShowQualityPage

class QualityPageViewTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        outputs1 = {
            "quality": {
                "details": {},
                "observing-conditions": "N/A",
                "sensitivity": "N/A",
                "summary": {
                    "L526107_SAP002_B073_P000_bf.tar": {
                        "added": [
                            "stokes/SAP2/BEAM73/L526105_SAP2_BEAM73_2bit.fits",
                            "stokes/SAP2/BEAM73/L526105_SAP2_BEAM73_2bit_ldv_psrfits_requantisation.log"
                        ],
                        "deleted": [
                            "stokes/SAP2/BEAM73/L526105_SAP2_BEAM73.fits"
                        ],
                        "input_name": "L526107_SAP002_B073_P000_bf.tar",
                        "input_size": 20353853440,
                        "input_size_str": "18.96 GB",
                        "output_name": "L526107_SAP002_B073_P000_bf.tar",
                        "output_size": 6024990720,
                        "output_size_str": "5.61 GB",
                        "rfi_percent": 11.167,
                        "size_ratio": 0.2960122876860019
                    }
                },
                "uv-coverage": "N/A"
            },
        }

        outputs2 = {
            "quality": {
                "details": {},
                "observing-conditions": "N/A",
                "sensitivity": "N/A",
                "summary": {
                    "L526107_SAP002_B073_P000_bf.tar": {
                        "added": [
                            "stokes/SAP2/BEAM73/L526105_SAP2_BEAM73_2bit.fits",
                            "stokes/SAP2/BEAM73/L526105_SAP2_BEAM73_2bit_ldv_psrfits_requantisation.log"
                        ],
                        "deleted": [
                            "stokes/SAP2/BEAM73/L526105_SAP2_BEAM73.fits"
                        ],
                        "input_name": "L526107_SAP002_B073_P000_bf.tar",
                        "input_size": 20353853440,
                        "input_size_str": "18.96 GB",
                        "output_name": "L526107_SAP002_B073_P000_bf.tar",
                        "output_size": 6024990720,
                        "output_size_str": "5.61 GB",
                        "rfi_percent": 11.167,
                        "size_ratio": 0.2960122876860019
                    }
                },
                "uv-coverage": "N/A"
            },
        }

        # Set up non-modified objects used by all test methods
        workflow = Workflow()
        workflow.save()

        # create a list of Tasks
        Task.objects.get_or_create(sas_id=123, workflow = workflow, outputs = outputs1)
        Task.objects.get_or_create(sas_id=456, workflow = workflow, outputs = outputs2)
        Task.objects.get_or_create(sas_id=789, workflow = workflow)

    def test_url_exists_at_desired_location(self):
        response = self.client.get('/atdb/quality')
        self.assertEqual(response.status_code, 200)

    def test_url_accessible_by_name(self):
        response = self.client.get(reverse('quality'))
        self.assertEqual(response.status_code, 200)

    def test_uses_correct_template(self):
        response = self.client.get(reverse('quality'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'taskdatabase/quality/page.html')

    # def test_queryset_for_tasks_with_quality(self):
    #     # this builds up the request and the session
    #     request = RequestFactory().get('/atdb/quality')
    #     middleware = SessionMiddleware()
    #     middleware.process_request(request)
    #     request.session.save()
    #
    #     # access the class in views.py and its overridden get_queryset method
    #     view = ShowQualityPage()
    #     view.request = request
    #     qs = view.get_queryset().object_list
    #     sum_sasid_actual = 0
    #     for task in qs:
    #         sum_sasid_actual += task.id
    #
    #     # test against the list of (2) test tasks with quality information
    #     tasks_with_quality = Task.objects.all().exclude(outputs__quality__isnull=True)
    #     sum_sasid_test = 0
    #     for task in tasks_with_quality:
    #         sum_sasid_test += task.id
    #
    #     self.assertEqual(sum_sasid_actual, sum_sasid_test)
