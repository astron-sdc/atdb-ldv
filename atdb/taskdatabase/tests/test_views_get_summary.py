from django.test import TestCase
from django.urls import reverse
from django.http import JsonResponse, HttpResponse

from taskdatabase.models import Task, Workflow, Activity
import taskdatabase.tests.test_calculated_qualities_outputs as outputs
import json

class TestGetSummary(TestCase):
    def setUp(self):

        workflow_requantisation = Workflow(workflow_uri="psrfits_requantisation")
        workflow_requantisation.save()

        activity_54321 = Activity(sas_id=54321, calculated_quality="good")
        activity_54321.save()

        # rfi_percent=0
        Task.objects.get_or_create(sas_id=54321, status='stored', activity = activity_54321,
                                   outputs=outputs.default_summary_flavour_with_rfi_percent_zero_1,
                                   workflow=workflow_requantisation,
                                   calculated_qualities = {"per_task": "good", "per_sasid": "good"},
        )

        # default summary flavour
        Task.objects.get_or_create(sas_id=54321, status='stored', outputs=outputs.default_summary_flavour_with_rfi_1,
                                   workflow=workflow_requantisation,
                                   calculated_qualities = {"per_task": "good", "per_sasid": "good"})
        Task.objects.get_or_create(sas_id=54321, status='stored', outputs=outputs.default_summary_flavour_with_rfi_2,
                                   workflow=workflow_requantisation,
                                   calculated_qualities = {"per_task": "good", "per_sasid": "good"})
        Task.objects.get_or_create(sas_id=54321, status='stored', outputs=outputs.default_summary_flavour_with_rfi_3,
                                   workflow=workflow_requantisation,
                                   calculated_qualities = {"per_task": "good", "per_sasid": "good"})
        Task.objects.get_or_create(sas_id=54321, status='stored', outputs=outputs.default_summary_flavour_with_rfi_4,
                                   workflow=workflow_requantisation,
                                   calculated_qualities = {"per_task": "good", "per_sasid": "good"})


        # test image compression, rfi_percentage=1.7186448587105623
        workflow_imaging_compression = Workflow(workflow_uri="imaging_compress_pipeline_v011")
        workflow_imaging_compression.save()

        activity_55555 = Activity(sas_id=55555, calculated_quality="good")
        activity_55555.save()

        Task.objects.get_or_create(sas_id=55555, status='stored', activity = activity_55555,
                                   outputs=outputs.imaging_compression_summary_flavor_with_rfi_1,
                                   workflow=workflow_imaging_compression,
                                   calculated_qualities={"per_task": "good", "per_sasid": "good"},
                                   size_to_process = 1000,
                                   size_processed = 900)

        # LINC pipelines (no rfi_percent onboard yet)
        workflow_linc_calibrator = Workflow(workflow_uri="linc_calibrator_v4_2")
        workflow_linc_calibrator.save()
        Task.objects.get_or_create(sas_id=666666, status='stored',
                                   outputs=outputs.link_calibrator_summary_without_rfi,
                                   calculated_qualities={"per_task": "good", "per_sasid": "good"},
                                   workflow=workflow_linc_calibrator)

        workflow_linc_target = Workflow(workflow_uri="linc_target_v4_2")
        workflow_linc_target.save()
        Task.objects.get_or_create(sas_id=666667, status='stored',
                                   outputs=outputs.link_target_summary_without_rfi,
                                   calculated_qualities={"per_task": "good", "per_sasid": "good"},
                                   workflow=workflow_linc_target)


    def test_summary_json_response(self):
        # Mock request
        response = self.client.get(reverse('get-summary', args=['54321', 'json']))

        # Check if response is JsonResponse
        self.assertIsInstance(response, JsonResponse)

    def test_summary_json_contents(self):
        response = self.client.get(reverse('get-summary', args=['54321', 'json']))

        # Check if response is JsonResponse
        self.assertIsInstance(response, JsonResponse)

        # Add more assertions as needed
        json_data = json.loads(response.content.decode('utf-8'))

        # is this json generated for the expected SAS_ID?
        expected = "Summary File for SAS_ID 54321"
        actual = json_data['summary']['title']
        self.assertEqual(expected, actual)

        # are all the tasks in the json?
        tasks = json_data['summary']['tasks']
        actual = len(tasks)
        expected = 5
        self.assertEqual(expected, actual)

        # check 1 task for correct contents
        t = tasks[0]
        self.assertEqual(t['file'], 'L526107_summaryIS.tar')
        self.assertEqual(t['input_name'], 'L526107_summaryIS.tar')
        self.assertEqual(t['input_size'], 495749120)
        self.assertEqual(t['output_size'], 283791360)
        self.assertEqual(t['size_ratio'], '0.572')

    def test_summary_json_contents_linc(self):
        response = self.client.get(reverse('get-summary', args=['666666', 'json']))

        # Check if response is JsonResponse
        self.assertIsInstance(response, JsonResponse)

        # Add more assertions as needed
        json_data = json.loads(response.content.decode('utf-8'))

        # is this json generated for the expected SAS_ID?
        expected = "Summary File for SAS_ID 666666"
        actual = json_data['summary']['title']
        self.assertEqual(expected, actual)

        # are all the tasks in the json?
        tasks = json_data['summary']['tasks']
        actual = len(tasks)
        expected = 1
        self.assertEqual(expected, actual)

        # check 1 task for correct contents
        t = tasks[0]
        self.assertEqual(t['basename'], '3c48_LINC_calibrator_summary.json')
        self.assertEqual(t['class'], 'File')
        self.assertEqual(t['checksum'], 'sha1$531646ff527d76f4facdabf72d939bac302eaf1f')
        self.assertEqual(t['location'], 'file:///project/ldv/Share/run/2023/6/16/1352_35011/3c48_LINC_calibrator_summary.json')
        self.assertEqual(t['surl'], 'srm://srm.grid.sara.nl/pnfs/grid.sara.nl/data/lofar/ops/disk/ldv/lt10_010/689478/35011/3c48_LINC_calibrator_summary.json')

    def test_summary_html_response(self):
        # Mock request
        response = self.client.get(reverse('get-summary', args=['your_sas_id', 'html']))

        # Check if response is HttpResponse
        self.assertIsInstance(response, HttpResponse)


    def test_summary_html_contents(self):
        # Arrange
        expected_title = "Summary File for SAS_ID 54321"
        expected_file = "L526107_summaryIS.tar"

        # Act
        response = self.client.get(reverse('get-summary', args=['54321', 'html']))

        # Check if response is JsonResponse
        self.assertIsInstance(response, HttpResponse)

        # Add more assertions as needed
        html_data = response.content.decode('utf-8')

        # Assert
        # test a little bit of the html content
        self.assertEqual(expected_title in html_data, True)
        self.assertEqual(expected_file in html_data, True)


    def test_summary_pdf_response(self):
        # Mock request
        response = self.client.get(reverse('get-summary', args=['54321', 'pdf']))

        # Check if response is HttpResponse
        self.assertIsInstance(response, HttpResponse)


    def test_summary_linc_html_contents(self):
        expected_title = "Summary File for SAS_ID 666666"


        # Act
        response = self.client.get(reverse('get-summary', args=['666666', 'html']))

        # Check if response is JsonResponse
        self.assertIsInstance(response, HttpResponse)

        # Add more assertions as needed
        html_data = response.content.decode('utf-8')

        # Assert
        # test a little bit of the html content
        self.assertEqual(expected_title in html_data, True)


    def test_summary_imaging_html_contents(self):
        expected_title = "Summary File for SAS_ID 55555"

        # Act
        response = self.client.get(reverse('get-summary', args=['55555', 'html']))

        # Check if response is JsonResponse
        self.assertIsInstance(response, HttpResponse)

        # Add more assertions as needed
        html_data = response.content.decode('utf-8')

        # Assert
        # test a little bit of the html content
        self.assertEqual(expected_title in html_data, True)

