from django.test import TestCase
import json

from taskdatabase.models import Workflow,Task, Activity


class TestJoinedTasks(TestCase):

    @classmethod
    def setUpTestData(cls):

        workflow_requantisation = Workflow(id=22, workflow_uri="psrfits_requantisation")
        workflow_requantisation.save()
        activity_12345 = Activity.objects.create(sas_id=12345)
        activity_12345.save()

        # create a list of Tasks
        Task.objects.get_or_create(sas_id=1, status='stored', workflow=workflow_requantisation, activity = activity_12345)
        Task.objects.get_or_create(sas_id=2, status='stored',workflow=workflow_requantisation, activity = activity_12345)
        Task.objects.get_or_create(sas_id=3, status='defined',workflow=workflow_requantisation, activity = activity_12345)
        Task.objects.get_or_create(sas_id=4, status='defined',workflow=workflow_requantisation, activity = activity_12345)

    def test_add_input_tasks_to_task(self):
        output_task = Task.objects.get(sas_id=1)
        input_task_1 = Task.objects.get(sas_id=2)
        input_task_2 = Task.objects.get(sas_id=3)
        output_task.joined_input_tasks.set([input_task_1,input_task_2])
        output_task.save()

        # count number of input asks
        count_input_tasks = output_task.joined_input_tasks.count()
        self.assertEqual(count_input_tasks,2)


    def test_join_status_none(self):
        output_task = Task.objects.get(sas_id=1)
        input_task_1 = Task.objects.get(sas_id=2) # stored
        input_task_2 = Task.objects.get(sas_id=3) # defined
        output_task.joined_input_tasks.set([input_task_1,input_task_2])
        output_task.save()

        # one input task on stored, and on on defined, should give: None

        self.assertEqual(output_task.joined_status,None)


    def test_join_status_stored(self):
        output_task = Task.objects.get(sas_id=3)
        input_task_1 = Task.objects.get(sas_id=2)  # stored
        input_task_2 = Task.objects.get(sas_id=1)  # stored
        output_task.joined_input_tasks.set([input_task_1, input_task_2])
        output_task.save()

        # two input tasks 'stored', should give: 'stored'
        self.assertEqual(output_task.joined_status, "stored")

    def test_is_task_type_join(self):
        output_task = Task.objects.get(sas_id=3)
        input_task_1 = Task.objects.get(sas_id=2)  # stored
        input_task_2 = Task.objects.get(sas_id=1)  # stored
        regular_task_3 = Task.objects.get(sas_id=4)
        output_task.joined_input_tasks.set([input_task_1, input_task_2])
        output_task.save()

        # the input task should be of the type 'join'
        self.assertEqual(input_task_1.task_type_join, "join")

        # the output task should be of the type 'joined'
        self.assertEqual(output_task.task_type_join, "joined")

        # a task without input/output tasks should be 'regular'
        self.assertEqual(regular_task_3.task_type_join, "regular")