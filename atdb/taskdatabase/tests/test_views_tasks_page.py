from django.test import TestCase
from django.urls import reverse

from taskdatabase.models import Task, Workflow

class TaskListViewTest(TestCase):

    @classmethod
    def setUpTestData(cls):

        # Set up non-modified objects used by all test methods
        workflow = Workflow()
        workflow.save()

        # create a list of Tasks
        Task.objects.get_or_create(sas_id=12345, workflow = workflow)
        Task.objects.get_or_create(sas_id=12345, workflow = workflow)
        Task.objects.get_or_create(sas_id=12345, workflow = workflow)

    def test_url_exists_at_desired_location(self):
        response = self.client.get('/atdb/')
        self.assertEqual(response.status_code, 200)

    def test_url_accessible_by_name(self):
        response = self.client.get(reverse('index'))
        self.assertEqual(response.status_code, 200)

    def test_uses_correct_template(self):
        response = self.client.get(reverse('index'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'taskdatabase/index.html')

