from django.test import TestCase
import json
from taskdatabase.models import Task, Workflow, Activity
from taskdatabase.services.common import State


class TestServiceFilter(TestCase):

    def setUp(self):
        """
        initialize test data
        """

        inputs = [
            {
                "size": 121047040,
                "surl": "srm://lofar-srm.fz-juelich.de:8443/pnfs/fz-juelich.de/data/lofar/ops/projects/lt16_004/2007949/L2007949_SAP000_SB000_uv.MS_a0a96273.tar",
                "type": "File",
                "location": "lofar-srm.fz-juelich.de"
            },
            {
                "size": 121077760,
                "surl": "srm://lofar-srm.fz-juelich.de:8443/pnfs/fz-juelich.de/data/lofar/ops/projects/lt16_004/2007949/L2007949_SAP000_SB001_uv.MS_783e9a94.tar",
                "type": "File",
                "location": "lofar-srm.fz-juelich.de"
            }]

        self.workflow_imaging_compression = Workflow(id=28, workflow_uri="imaging_compression")
        self.workflow_imaging_compression.save()

        self.regular_task = Task.objects.create(sas_id=113,
                                                task_type = 'regular',
                                                new_status='defined',
                                                workflow=self.workflow_imaging_compression,
                                                inputs=inputs)

        self.aggregation_task = Task.objects.create(sas_id=113,
                                         new_status='idle',
                                         task_type = 'aggregation',
                                         workflow=self.workflow_imaging_compression)

        self.unknown_task_of_known_activity = Task.objects.create(sas_id=113,
                                         new_status='defined',
                                         task_type = 'whatever',
                                         workflow=self.workflow_imaging_compression)

        self.unknown_task = Task.objects.create(sas_id=114,
                                         new_status='defining',
                                         task_type = 'whatever',
                                         workflow=self.workflow_imaging_compression)

    def test_regular_task_service_filter_from_inputs(self):
        # the task.service_filter should be extracted from the task.inputs.location value

        # arrange
        expected_task_service_filter = 'lofar-srm.fz-juelich.de'

        # act
        task_service_filter = self.regular_task.service_filter

        # assert
        self.assertEqual(task_service_filter,expected_task_service_filter)

    def test_activity_service_filter_from_task(self):
        # the activity.service_filter should be extracted from the task.service_filter value

        # arrange
        expected_activity_service_filter = 'lofar-srm.fz-juelich.de'

        # act
        activity_service_filter = self.regular_task.activity.service_filter

        # assert
        self.assertEqual(activity_service_filter,expected_activity_service_filter)

    def test_aggregation_task_service_filter_from_sibling(self):
        # aggregation tasks have no inputs, they get their service_filter from an earlier task of the same sas_id
        # (that earlier task exists per definition, because aggregation tasks are created from them)

        # arrange
        expected_task_service_filter = 'lofar-srm.fz-juelich.de'

        # act
        task_service_filter = self.aggregation_task.service_filter

        # assert
        self.assertEqual(task_service_filter,expected_task_service_filter)

    def test_unknown_task_service_filter_from_activity(self):
        # unknown task types without inputs can be added through the API
        # if they are of a SAS_ID that already have regular tasks onboard,
        # then they can get their service_filter from the activity.

        # arrange
        expected_task_service_filter = 'lofar-srm.fz-juelich.de'

        # act
        task_service_filter = self.unknown_task_of_known_activity.service_filter

        # assert
        self.assertEqual(task_service_filter,expected_task_service_filter)

    def test_unknown_task_service_filter_from_activity(self):
        # unknown task types without inputs can be added through the API
        # if they are of a SAS_ID that already have regular tasks onboard,
        # then they can get their service_filter from the activity.

        # arrange
        expected_task_service_filter = 'lofar-srm.fz-juelich.de'

        # act
        task_service_filter = self.unknown_task_of_known_activity.service_filter

        # assert
        self.assertEqual(task_service_filter,expected_task_service_filter)

    def test_updated_service_filter(self):
        # unknown task types without inputs can be added through the API
        # if they do not have siblings or an activity, then no service_filter can be determined

        # arrange
        self.regular_task.service_filter = "updated_filter"
        expected_task_service_filter = "updated_filter"
        expected_activity_service_filter = "lofar-srm.fz-juelich.de"

        # act
        task_service_filter = self.regular_task.service_filter
        activity_service_filter = self.regular_task.activity.service_filter

        # assert
        self.assertEqual(task_service_filter,expected_task_service_filter)
        self.assertEqual(activity_service_filter,expected_activity_service_filter)
