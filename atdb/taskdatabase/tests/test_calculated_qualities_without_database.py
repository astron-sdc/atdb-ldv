import unittest
from taskdatabase.services import calculated_qualities as qualities
class TestCalculatedQualities(unittest.TestCase):

    moderate_treshold = 20
    poor_treshold = 50

    def test_rfi_percentage_to_quality_None(self):
        quality = qualities.rfi_percentage_to_quality(-10, TestCalculatedQualities.moderate_treshold, TestCalculatedQualities.poor_treshold)
        self.assertIsNone(quality)

    def test_rfi_percentage_to_quality_Zero(self):
        quality = qualities.rfi_percentage_to_quality(0, TestCalculatedQualities.moderate_treshold,
                                                      TestCalculatedQualities.poor_treshold)
        self.assertIsNotNone(quality)

    def test_rfi_percentage_to_quality_Poor(self):
        quality = qualities.rfi_percentage_to_quality(60, TestCalculatedQualities.moderate_treshold, TestCalculatedQualities.poor_treshold)
        self.assertEqual(quality, "poor")

    def test_rfi_percentage_to_quality_Moderate(self):
        quality = qualities.rfi_percentage_to_quality(25, TestCalculatedQualities.moderate_treshold, TestCalculatedQualities.poor_treshold)
        self.assertEqual(quality, "moderate")

    def test_rfi_percentage_to_quality_Good(self):
        quality = qualities.rfi_percentage_to_quality(19, TestCalculatedQualities.moderate_treshold, TestCalculatedQualities.poor_treshold)
        self.assertEqual(quality, "good")


