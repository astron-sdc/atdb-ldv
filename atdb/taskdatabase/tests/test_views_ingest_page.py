from django.test import TestCase
from django.urls import reverse

from taskdatabase.models import Task, Workflow
class IngestPageViewTest(TestCase):

    @classmethod
    def setUpTestData(cls):

        # Set up non-modified objects used by all test methods
        workflow = Workflow()
        workflow.save()

        # create a list of Tasks
        Task.objects.get_or_create(sas_id=12345, status='scrubbed', workflow = workflow)
        Task.objects.get_or_create(sas_id=12345, status='scrubbed', workflow = workflow)
        Task.objects.get_or_create(sas_id=12345, status='archiving', workflow = workflow)
        Task.objects.get_or_create(sas_id=12345, status='finished', workflow=workflow)

    def test_url_exists_at_desired_location(self):
        response = self.client.get('/atdb/ingest')
        self.assertEqual(response.status_code, 200)

    def test_url_accessible_by_name(self):
        response = self.client.get(reverse('ingest'))
        self.assertEqual(response.status_code, 200)

    def test_uses_correct_template(self):
        response = self.client.get(reverse('ingest'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'taskdatabase/ingest/page.html')