from django.test import TestCase
from django.urls import reverse

from taskdatabase.models import LatestMonitor
class MonitoringPageViewTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        # create a list of Monitor objects
        LatestMonitor.objects.get_or_create(name="stager", status='running')
        LatestMonitor.objects.get_or_create(name="archiver", status='running')
        LatestMonitor.objects.get_or_create(name="executor", status='running')
        LatestMonitor.objects.get_or_create(name="datamanager", status='running')
        LatestMonitor.objects.get_or_create(name="token", status='running')
        LatestMonitor.objects.get_or_create(name="cleanup", status='running')
        LatestMonitor.objects.get_or_create(name="cleanup-spider", status='running')
    def test_url_exists_at_desired_location(self):
        response = self.client.get('/atdb/monitoring/')
        self.assertEqual(response.status_code, 200)

    def test_url_accessible_by_name(self):
        response = self.client.get(reverse('monitoring'))
        self.assertEqual(response.status_code, 200)

    def test_uses_correct_template(self):
        response = self.client.get(reverse('monitoring'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'taskdatabase/monitoring_page.html')