from django.test import TestCase
from taskdatabase.models import Task, Workflow, Activity

class PathToLTATest(TestCase):
    def setUp(self):
        self.workflow_requantisation = Workflow(id=22, workflow_uri="psrfits_requantisation")
        self.workflow_requantisation.save()
        self.activity_12345 = Activity.objects.create(sas_id=12345, archive={'path_to_lta': '/sample/path', 'sas_id_archived': 54321})
        self.activity_12345.save()
        self.activity_66666 = Activity.objects.create(sas_id=66666, archive={})
        self.activity_66666.save()

        # the first 2 have no valid path set
        self.task1 = Task.objects.create(sas_id=12345,archive={}, workflow=self.workflow_requantisation, activity = self.activity_12345)
        self.task2 = Task.objects.create(sas_id=12345,archive={'path_to_lta': None}, workflow=self.workflow_requantisation, activity = self.activity_12345)

        # this task has a valid path_to_lta set
        self.task3 = Task.objects.create(sas_id=12345,archive={'path_to_lta': '/sample/path', 'sas_id_archived': 54321}, workflow=self.workflow_requantisation, activity = self.activity_12345)

        # this sasid has no path_to_lta set at all
        self.task4 = Task.objects.create(sas_id=66666,archive={}, workflow=self.workflow_requantisation, activity = self.activity_66666)
        self.task5 = Task.objects.create(sas_id=66666,archive={}, workflow=self.workflow_requantisation, activity = self.activity_66666)

    def test_path_to_lta_with_path(self):
        # if only one of the tasks has a path_to_lta, then the other tasks should also return that path
        for task in Task.objects.filter(sas_id=12345):
            result = task.activity.archive['path_to_lta']
            self.assertEqual(result, '/sample/path')

    def test_path_to_lta_without_path(self):
        # if one of the tasks has 'path_to_lta' set, then return None
        for task in Task.objects.filter(sas_id=66666):
            result = task.path_to_lta
            self.assertEqual(result, None)

    def test_has_archived(self):
        # if only one of the tasks has a sas_id_has_archived, then the other tasks should also return that path
        for task in Task.objects.filter(sas_id=12345):
            result = task.activity.has_archived
            self.assertEqual(result, 54321)