from django.test import TestCase
from django.urls import reverse

from taskdatabase.models import Task, Workflow
class DashboardPageViewTest(TestCase):

    @classmethod
    def setUpTestData(cls):

        # Set up non-modified objects used by all test methods
        workflow = Workflow()
        workflow.save()

        # create a list of Tasks
        Task.objects.get_or_create(sas_id=12345, status='finished', workflow = workflow)
        Task.objects.get_or_create(sas_id=12345, status='finished', workflow = workflow)
        Task.objects.get_or_create(sas_id=12345, status='stored', workflow = workflow)

    def test_url_exists_at_desired_location_active_nores_nofilter(self):
        response = self.client.get('/atdb/dashboard/active_nores_nofilter')
        self.assertEqual(response.status_code, 200)

    def test_url_exists_at_desired_location_all_resources_applyfilter(self):
        response = self.client.get('/atdb/dashboard/all_resources_applyfilter')
        self.assertEqual(response.status_code, 200)

    def test_uses_correct_template(self):
        response = self.client.get('/atdb/dashboard/active_nores_nofilter')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'taskdatabase/dashboard/dashboard.html')