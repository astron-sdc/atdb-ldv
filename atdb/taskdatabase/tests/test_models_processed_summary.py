from django.test import TestCase
import json
from taskdatabase.models import Configuration, Task, Workflow, Activity
from taskdatabase.services.common import State

class TestProcessedSummary(TestCase):

    def setUp(self):
        """
        initialize test data
        """
        Configuration.objects.create(key="executor:workdir", value="/project/ldv/Share/run/")

        self.workflow_requantisation = Workflow(id=22, workflow_uri="psrfits_requantisation",
                                                aggregation_strategy="wait_for_summary_task")
        self.workflow_requantisation.save()

        self.workflow_imaging_compression = Workflow(id=14, workflow_uri="imaging_compression",
                                                aggregation_strategy="collect_h5")
        self.workflow_imaging_compression.save()

        self.task1 = Task.objects.create(sas_id=223,
                                         filter="test_blabla",
                                         new_status=State.PROCESSED.value,
                                         workflow=self.workflow_requantisation,
                                         is_summary=False)
        self.task1.save()

        # this is a summary task (old style)
        self.task2 = Task.objects.create(sas_id=222,
                                         new_status=State.PROCESSED.value,
                                         workflow=self.workflow_requantisation,
                                         outputs={"tar_archive": [{"size": 4885985280, "basename": "L185619_summaryCS.tar", "nameroot": "L185619_summaryCS"}]})
        self.task2.save()

        # this is a summary task (new style, using ís_summary)
        self.task3 = Task.objects.create(sas_id=222,
                                         new_status=State.PROCESSED.value,
                                         workflow=self.workflow_requantisation,
                                         outputs={
                                             "summary": [{"is_summary": True}],
                                             "quality": {
                                                 "summary" : {"L441006_summaryCS.tar" : {"is_summary" : True} },
                                                 "plots" : [{"basename": "L441006_CS_quick_summary.pdf"}]
                                             }
                                         })

        self.task3.save()

        # this is not a summary task

        self.task4 = Task.objects.create(sas_id=222,
                                         filter="test_blabla",
                                         new_status=State.PROCESSED.value,
                                         workflow=self.workflow_requantisation,
                                         is_summary=False)
        self.task4.save()

        # this is a summary task, but it uses a workflow with an aggregation_strategy that should not hold the task
        self.task5 = Task.objects.create(sas_id=333,
                                         new_status=State.PROCESSED.value,
                                         workflow=self.workflow_imaging_compression,
                                         outputs={"summary": [{"is_summary": True}]})
        self.task5.save()



    def test_missing_summary_task_processed_on_hold(self):
        """
        task 1 is not initially a summary dataproduct. Usually these should NOT go on hold
        Unless the aggregation strategy demands that there must be 1 summary task on hold and no summary task was found.
        This test simulates that corner case (see bugfix SDC-1511)
        """

        actual = self.task1.resume
        self.assertFalse(actual)

    def test_non_summary_processed_not_on_hold(self):
        """
        task 1 is processed, but not a summary dataproduct. Should NOT go on hold
        """

        actual = self.task4.resume
        self.assertTrue(actual)

    def test_processed_not_on_hold_for_different_strategy(self):
        """
        this is a summary task, but it uses a workflow with an aggregation_strategy that should not hold the task
        """
        actual = self.task5.resume
        self.assertTrue(actual)

    def test_processed_on_hold(self):
        """
        task 2 is processed, and a summary dataproduct. Should go on hold
        """

        actual = self.task2.resume
        self.assertFalse(actual)


    def test_activity_is_processed(self):
        """
        both tasks are processed, the activity should have the is_processed flag now
        """

        actual = self.task1.activity.is_processed
        self.assertTrue(actual)

    def test_has_summary_substring(self):
        """
        task 2 only has the old summary filename test. Check if the task indeed gets seen as a summary_task
        """
        actual = self.task2.is_summary
        self.assertTrue(actual)

    def test_is_summary(self):
        """
        task 3 only has the new 'is_summary' test. Check if the task indeed gets seen as a summary_task
        """
        actual = self.task3.is_summary
        self.assertTrue(actual)

    def test_has_quality_summary(self):
        """
        task 3 has both the quality.summary field and summary field filled
        It is the quality.summary field that is used as the real source of truth.
        """
        actual = self.task3.has_summary
        self.assertTrue(actual)

    def test_has_plots(self):
        """
        task 3 has quality.plots field to test the has_plots function
        """
        actual = self.task3.has_plots
        self.assertTrue(actual)

    def test_has_no_plots(self):
        """
        task 4 has no quality.plots field to test the has_plots function
        """
        actual = self.task5.has_plots
        self.assertFalse(actual)
    def test_predecessor_status(self):
        """
        test prececessor_status
        """
        actual = self.task3.predecessor_status
        self.assertEqual(actual, "no_predecessor")