from django.test import TestCase
from django.utils import timezone
from datetime import datetime
from taskdatabase.models import LogEntry, Task, Workflow, Job, Configuration
from taskdatabase.services import algorithms
from unittest.mock import Mock, MagicMock, patch

class TestAlgorithms(TestCase):

    def setUp(self):

        # used to test the get_size calculation, this uses a database
        self.workflow = Workflow(id=22, workflow_uri="psrfits_requantisation")
        self.workflow.save()

        self.plots1 = [{
                    "size": 8545496,
                    "surl": "srm://srm.grid.sara.nl/pnfs/grid.sara.nl/data/lofar/ops/disk/ldv/lc4_030/346406/382968/L346406_CS_quick_summary.pdf",
                    "class": "File",
                    "nameext": ".pdf",
                    "basename": "L346406_CS_quick_summary.pdf",
                    "checksum": "sha1$fa70088209bc7c61f7d3e3f9594c84ed1b2316ca",
                    "location": "file:///project/ldv/Share/run/2024/8/3/1557_382968/L346406_CS_quick_summary.pdf",
                    "nameroot": "L346406_CS_quick_summary",
                    "surl_lta": "srm://srm.grid.sara.nl/pnfs/grid.sara.nl/data/lofar/ops/disk/projects/lc4_030/1396673/L346406_CS_quick_summary.pdf"
                },{
                    "size": 30720,
                    "surl": "srm://srm.grid.sara.nl/pnfs/grid.sara.nl/data/lofar/ops/disk/ldv/lc4_030/346406/382968/L346406_CS_misc.tar",
                    "class": "File",
                    "nameext": ".tar",
                    "basename": "L346406_CS_misc.tar",
                    "checksum": "sha1$322845b992ed0359144195314b6ee99f06077617",
                    "location": "file:///project/ldv/Share/run/2024/8/3/1557_382968/L346406_CS_misc.tar",
                    "nameroot": "L346406_CS_misc",
                    "surl_lta": "srm://srm.grid.sara.nl/pnfs/grid.sara.nl/data/lofar/ops/disk/projects/lc4_030/1396673/L346406_CS_misc.tar"
                }]

        self.plots2 = [{
            "size": 247434,
            "surl": "srm://srm.grid.sara.nl/pnfs/grid.sara.nl/data/lofar/ops/disk/ldv/lt5_004/558682/274440/L558680_SAP0_BEAM12_rfifind-0.png",
            "class": "File",
            "nameext": ".png",
            "basename": "L558680_SAP0_BEAM12_rfifind-0.png",
            "checksum": "sha1$bf1304b4642ec3ce80736d255e04deab7d60ca16",
            "location": "file:///project/ldv/Share/run/2024/4/20/033_274440/L558680_SAP0_BEAM12_rfifind-0.png",
            "nameroot": "L558680_SAP0_BEAM12_rfifind-0",
            "surl_lta": "srm://srm.grid.sara.nl/pnfs/grid.sara.nl/data/lofar/ops/disk/projects/lt5_004/1277900/L558680_SAP0_BEAM12_rfifind-0.png"
        },
        {
            "size": 66215,
            "surl": "srm://srm.grid.sara.nl/pnfs/grid.sara.nl/data/lofar/ops/disk/ldv/lt5_004/558682/274440/L558680_SAP0_BEAM12_rfifind-1.png",
            "class": "File",
            "nameext": ".png",
            "basename": "L558680_SAP0_BEAM12_rfifind-1.png",
            "checksum": "sha1$876034afdbdf3e1437775bacace750744be5484d",
            "location": "file:///project/ldv/Share/run/2024/4/20/033_274440/L558680_SAP0_BEAM12_rfifind-1.png",
            "nameroot": "L558680_SAP0_BEAM12_rfifind-1",
            "surl_lta": "srm://srm.grid.sara.nl/pnfs/grid.sara.nl/data/lofar/ops/disk/projects/lt5_004/1277900/L558680_SAP0_BEAM12_rfifind-1.png"
        }]


        self.task1 = Task.objects.create(sas_id='5432', status='processed', new_status='processed',
                                         size_processed = 123, size_to_process = 100, workflow=self.workflow,
                                         outputs={"quality": {"plots" : self.plots1}})
        self.task1.save()

        self.task2 = Task.objects.create(sas_id='5432', status='stored',new_status='stored',
                                         size_processed = 123, size_to_process = 100, workflow=self.workflow,
                                         outputs={"quality": {"plots" : self.plots2}})
        self.task2.save()

        # used to test the get_min_max calcuation
        # used to test logentry html tests
        self.logentry1 = LogEntry.objects.create(task = self.task1, step_name = 'running', status = 'processing',
                                                 service='aggregator',
                                                 cpu_cycles = 2000, wall_clock_time = 10000,
                                                 url_to_log_file = None,
                                                 description="Description1",
                                                 timestamp = datetime(2024, 8, 10, 14, 0, 0))
        self.logentry2 = LogEntry.objects.create(task = self.task1, step_name = 'running', status = 'processed',
                                                 service='aggregator',
                                                 cpu_cycles=1000, wall_clock_time=5000,
                                                 url_to_log_file="http://example.com/log1",
                                                 description="Description2",
                                                 timestamp = datetime(2024, 8, 10, 15, 0, 0))
        self.logentry3= LogEntry.objects.create(task = self.task2, step_name = 'running', status = 'processing',
                                                 timestamp = datetime(2024, 8, 10, 16, 0, 0))
        self.logentry4= LogEntry.objects.create(task = self.task2, step_name = 'running', status = 'processed',
                                                 timestamp = datetime(2024, 8, 10, 17, 0, 0))

        # used to test the generated html, this mocks the database
        self.task3 = Mock()
        self.task3.pk = 1
        self.task3.sas_id = "611454"
        self.task3.project = "lt5_004"
        self.task3.filter = "lt5_004-vk-2bits"
        self.task3.quality = "Good"
        self.task3.quality_json = {}

        self.task4 = Mock()
        self.task4.pk = 2
        self.task4.sas_id = "611454"
        self.task4.project = "lt5_004"
        self.task4.filter = "lt5_004-vk-2bits"
        self.task4.quality = "Good"
        self.task4.quality_json = {}
        self.task4.inputs = [
        {
            "size": 210913280,
            "surl": "srm://srm.grid.sara.nl:8443/pnfs/grid.sara.nl/data/lofar/ops/projects/lt5_004/558682/L558682_summaryIS_46f54cd2.tar",
            "type": "File",
            "location": "srm.grid.sara.nl"
        }],
        self.task4.metrics = {'metric1' : 'value1', 'metric2' : 'http://my_web_link'}


    def test_get_size(self):

        # arrange
        status_list = ["processed","stored"]
        type = "processed"

        expected_size = 246

        # act
        size = algorithms.get_size(status_list,type)

        # assert
        self.assertEqual(size, expected_size)


    def test_get_min_start_and_max_end_time(self):

        # Arrange
        expected_min = datetime(2024, 8, 10, 14, 0, 0)
        expected_max = datetime(2024, 8, 10, 17, 0, 0)

        # Act
        min_start_time, max_end_time = algorithms.get_min_start_and_max_end_time('5432')

        # get rid of the timezone
        min_start_time = min_start_time.replace(tzinfo=None)
        max_end_time = max_end_time.replace(tzinfo=None)

        # Assert
        self.assertEqual(min_start_time, expected_min)
        self.assertEqual(max_end_time, expected_max)


    def test_convert_logentries_to_html(self):
        log_entries = [self.logentry1, self.logentry2]

        # Arrange
        expected_html = ('<th>service</th><th>step</th><th>status</th><th width="200px">timestamp</th>'
                         '<th>cpu_cycles</th><th>wall_clock_time</th><th>log</th>'
                         '<tbody>'
                         '<tr><td><b>aggregator</b></td>'
                         '<td><b>running</b></td>'
                         '<td class="processing" >processing</td>'
                         '<td>08-10-2024, 14:00:00</td>'
                         '<td>2000</td>'
                         '<td>10000</td>'
                         '<td>Description1</td>'
                         '<tr><td><b>aggregator</b></td>'
                         '<td><b>running</b></td>'
                         '<td class="processed" >processed</td>'
                         '<td>08-10-2024, 15:00:00</td>'
                         '<td>1000</td>'
                         '<td>5000</td>'
                         '<td><a href="http://example.com/log1" target="_blank">logfile</a></td>'''
                         '</tbody>'
                         )
        # Act
        result = algorithms.convert_logentries_to_html(log_entries)

        # Assert
        self.assertEqual(result, expected_html)


    def test_basic_convert_quality_to_html(self):
        # Test basic HTML conversion
        expected_html = (
            "<tr><td><b>SAS_ID</b></td><td>611454</td></tr>"
            "<tr><td><b>Project</b></td><td>lt5_004</td></tr>"
            "<tr><td><b>ATDB Filter</b></td><td>lt5_004-vk-2bits</td></tr>"
            "<tr><td><b>Quality</b></td><td>Good</td></tr>"
        )
        result = algorithms.convert_quality_to_html(self.task3)
        self.assertIn(expected_html, result)


    def test_convert_quality_to_html_nested_json_fields(self):
        # Test with nested JSON fields in the task.quality_json
        self.task3.quality_json = {
            'uv-coverage': 'Good',
            'sensitivity': 'Moderate',
            'observing-conditions': 'Clear',
            'details': {
                'high_flagging': True,
                'elevation_score': 85,
                'sun_interference': 'Low',
                'moon_interference': 'None',
                'jupiter_interference': 'Minimal',
                'full_array_incomplete': False,
                'dutch_array_incomplete': False,
                'full_array_incomplete_is': False,
                'dutch_array_flag_data_loss': True,
                'dutch_array_high_data_loss': False,
                'fill_array_missing_is_pair': True,
                'full_array_missing_important_pair': False,
                'dutch_array_missing_important_pair': False,
                'dutch_array_high_data_loss_on_important_pair': True
            }
        }

        result = algorithms.convert_quality_to_html(self.task3)

        # Check if certain key fields are correctly converted
        self.assertIn("<tr><td><b>QA uv-coverage</b></td><td>Good</td></tr>", result)
        self.assertIn("<tr><td><b>elevation_score</b></td><td>85</td></tr>", result)
        self.assertIn("<tr><td><b>sun_interference</b></td><td>Low</td></tr>", result)
        self.assertIn("<tr><td><b>moon_interference</b></td><td>None</td></tr>", result)
        self.assertIn("<tr><td><b>jupiter_interference</b></td><td>Minimal</td></tr>", result)

    def test_convert_quality_to_html_missing_json_fields(self):
        # Test when some JSON fields are missing (function should not raise exceptions)

        # arrange
        self.task3.quality_json = {
            'uv-coverage': 'Good',
            'sensitivity': 'Moderate'
        }

        # act
        result = algorithms.convert_quality_to_html(self.task3)

        # assert
        # Check that the existing fields are present and that the missing ones don't break the function
        self.assertIn("<tr><td><b>QA uv-coverage</b></td><td>Good</td></tr>", result)
        self.assertIn("<tr><td><b>QA sensitivity</b></td><td>Moderate</td></tr>", result)


    def test_convert_quality_to_html_complete(self):
        # Comprehensive test with all fields filled

        # arrange
        self.task3.quality_json = {
            'uv-coverage': 'Good',
            'sensitivity': 'High',
            'observing-conditions': 'Clear',
            'details': {
                'high_flagging': False,
                'elevation_score': 95,
                'sun_interference': 'None',
                'moon_interference': 'Minimal',
                'jupiter_interference': 'Moderate',
                'full_array_incomplete': False,
                'dutch_array_incomplete': True,
                'full_array_incomplete_is': False,
                'dutch_array_flag_data_loss': True,
                'dutch_array_high_data_loss': True,
                'fill_array_missing_is_pair': False,
                'full_array_missing_important_pair': True,
                'dutch_array_missing_important_pair': False,
                'dutch_array_high_data_loss_on_important_pair': False
            }
        }

        # act
        result = algorithms.convert_quality_to_html(self.task3)

        # assert
        self.assertIn("<tr><td><b>SAS_ID</b></td><td>611454</td></tr>", result)
        self.assertIn("<tr><td><b>QA uv-coverage</b></td><td>Good</td></tr>", result)
        self.assertIn("<tr><td><b>dutch_array_incomplete</b></td><td>True</td></tr>", result)


    def test_construct_inspection_plots(self):
        # Test the rendering of png files for a task id, it should expand to a clickable webdav link

        # arrange
        expected_html = ('<h4>Inspection Plots and Summary Logs</h4>'
                         '<p>Clicking a link will redirect to SURF SARA in a new browser window. </p>'
                         '<tr><td><a href="https://webdav.grid.surfsara.nl/projects/lc4_030/1396673/L346406_CS_quick_summary.pdf?action=show&authz=" target="_blank">L346406_CS_quick_summary.pdf</a></td></tr>'
                         '<tr><td><a href="https://webdav.grid.surfsara.nl/projects/lc4_030/1396673/L346406_CS_misc.tar?action=show&authz=" target="_blank">L346406_CS_misc.tar</a></td></tr>')

        # act
        result = algorithms.construct_inspectionplots(self.task1)

        # assert
        self.assertEqual(expected_html, result)


    def test_construct_inspection_plots_expand_image(self):

        # arrange
        expected_html = ('<h4>Inspection Plots and Summary Logs</h4><p>Clicking a link will redirect to SURF SARA in a new browser window. </p>')
        # act
        result = algorithms.construct_inspectionplots(self.task2, expand_image=True)

        # assert
        self.assertEqual(expected_html, result)


    def test_construct_inspection_plots_sas_id(self):
        # Test the rendering of inspection plots for a sas_id

        # arrange
        expected_html = ('<h4>(Unique) Inspection Plots and Summary Logs for SAS_ID 5432</h4>'
                         '<p>Clicking a link will redirect to SURF SARA in a new browser window. </p>'
                         '<tr style="background-color:#7EB1C4"><td colspan="3"><b>Task 7</b></td></tr>'
                         '<tr><td><a href="https://webdav.grid.surfsara.nl/projects/lc4_030/1396673/L346406_CS_quick_summary.pdf?action=show&authz=" target="_blank">L346406_CS_quick_summary.pdf</a></td></tr>'
                         '<tr><td><a href="https://webdav.grid.surfsara.nl/projects/lc4_030/1396673/L346406_CS_misc.tar?action=show&authz=" target="_blank">L346406_CS_misc.tar</a></td></tr>'
                         '<tr style="background-color:#7EB1C4"><td colspan="3"><b>Task 8</b></td></tr>'
                         '<tr><td><a href="https://webdav.grid.surfsara.nl/projects/lt5_004/1277900/L558680_SAP0_BEAM12_rfifind-0.png?action=show&authz=" target="_blank">L558680_SAP0_BEAM12_rfifind-0.png</a></td></tr>'
                         '<tr><td><a href="https://webdav.grid.surfsara.nl/projects/lt5_004/1277900/L558680_SAP0_BEAM12_rfifind-1.png?action=show&authz=" target="_blank">L558680_SAP0_BEAM12_rfifind-1.png</a></td></tr>')

        # act
        result = algorithms.construct_inspectionplots(self.task1,source="sas_id")

        # assert
        self.assertEqual(expected_html, result)


    def test_convert_list_of_dicts_to_html(self):
        # arrange
        expected_html = '<tr><td><b>metric1</b></td><td>value1</td></tr><tr><td><b>metric2</b></td><td><a href="http://my_web_link">metric2</a></td></tr>'

        # act
        result = algorithms.convert_list_of_dicts_to_html(self.task4.metrics)

        # assert
        self.assertEqual(expected_html, result)


    def test_convert_json_to_nested_table(self):

        # arrange
        expected_html = ('<tbody><tr><td><table>'
                         '<tr><td><table><tr><td><b>size</b></td><td><td style="max-width:25rem">210913280</td></td></tr>'
                         '<tr><td><b>surl</b></td><td><td style="max-width:25rem">srm://srm.grid.sara.nl:8443/pnfs/grid.sara.nl/data/lofar/ops/projects/lt5_004/558682/L558682_summaryIS_46f54cd2.tar</td></td></tr>'
                         '<tr><td><b>type</b></td><td><td style="max-width:25rem">File</td></td></tr><tr><td><b>location</b></td><td><td style="max-width:25rem">srm.grid.sara.nl</td></td></tr>'
                         '</table></td></tr></table></td></tr></tbody>')

        # act
        result = algorithms.convert_json_to_nested_table(self.task4.inputs)

        # assert
        self.assertEqual(expected_html, result)


