from django.test import TestCase, RequestFactory
from django.urls import reverse
from unittest.mock import patch, MagicMock

from taskdatabase.models import Task, Workflow
from taskdatabase.views import ShowInspectionPlots, ShowInspectionPlotsSasId,ShowSummarySasId

class TestDetailsPage(TestCase):

    @classmethod
    def setUp(self):
        self.factory = RequestFactory()

        # Set up non-modified objects used by all test methods
        workflow = Workflow()
        workflow.save()

        # create a list of Tasks
        self.task1 = Task.objects.create(sas_id=12345, status='finished', workflow=workflow)

    def test_url_exists_at_desired_location(self):
        # extract the task
        response = self.client.get(f'/atdb/task_details/{self.task1.id}/1')
        self.assertEqual(response.status_code, 200)


    def test_uses_correct_template(self):
        response = self.client.get(f'/atdb/task_details/{self.task1.id}/1')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'taskdatabase/tasks/task_details.html')

    def test_show_inputs(self):
        response = self.client.get(f'/atdb/show-inputs/{self.task1.id}/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'taskdatabase/details/inputs.html')

    def test_show_outputs(self):
        response = self.client.get(f'/atdb/show-outputs/{self.task1.id}/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'taskdatabase/details/outputs.html')

    def test_show_metrics(self):
        response = self.client.get(f'/atdb/show-metrics/{self.task1.id}/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'taskdatabase/details/metrics.html')

    @patch('taskdatabase.views.algorithms.construct_inspectionplots')
    def test_show_inspectionplots(self,mock_plots):
        # Arrange
        mock_plots.return_value = "<p>Inspection Plots HTML</p>"
        # Simulate a GET request to the view
        request = self.factory.get('/show_inspectionplots/')

        # Act
        response = ShowInspectionPlots(request, id=self.task1.id)

        # Assert
        self.assertEqual(response.status_code, 200)

        # Assert that the my_plots context contains the mocked HTML
        self.assertIn('Inspection Plots HTML', response.content.decode())

    @patch('taskdatabase.views.algorithms.construct_inspectionplots')
    def test_show_inspectionplots_sasid(self,mock_plots):
        # Arrange
        mock_plots.return_value = "<p>Inspection Plots HTML</p>"
        # Simulate a GET request to the view
        request = self.factory.get('/show_inspectionplots_sasid/')

        # Act
        response = ShowInspectionPlotsSasId(request, id=self.task1.id)

        # Assert
        self.assertEqual(response.status_code, 200)

        # Assert that the my_plots context contains the mocked HTML
        self.assertIn('Inspection Plots HTML', response.content.decode())

    @patch('taskdatabase.views.algorithms.construct_summary')
    def test_show_summary_sasid(self,mock_summary):
        # Arrange
        mock_summary.return_value = "<p>Summary HTML</p>"
        # Simulate a GET request to the view
        request = self.factory.get('/show_summary/')

        # Act
        response = ShowSummarySasId(request, id=self.task1.id)

        # Assert
        self.assertEqual(response.status_code, 200)

        # Assert that the my_plots context contains the mocked HTML
        self.assertIn('<p>Summary HTML</p>', response.content.decode())