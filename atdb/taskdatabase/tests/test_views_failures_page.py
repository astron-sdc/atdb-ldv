from django.test import TestCase
from django.urls import reverse

from taskdatabase.models import Task, Workflow
class FailuresPageViewTest(TestCase):


    def test_url_exists_at_desired_location(self):
        response = self.client.get('/atdb/failures')
        self.assertEqual(response.status_code, 200)

    def test_url_accessible_by_name(self):
        response = self.client.get(reverse('failures'))
        self.assertEqual(response.status_code, 200)

    def test_uses_correct_template(self):
        response = self.client.get(reverse('failures'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'taskdatabase/failures/page.html')