from django.test import TestCase
from django.urls import reverse
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.test import Client

from taskdatabase.models import Task, Workflow

class ChangePriorityViewTest(TestCase):
    def setUp(self):
        # Create a test user
        self.user = User.objects.create_user(username='testuser', password='testpassword')

        # Create a test workflow
        workflow_requantisation = Workflow(workflow_uri="psrfits_requantisation")
        workflow_requantisation.save()

        # Create a test task
        self.task = Task.objects.create(filter='a',sas_id=54321, status='stored', workflow=workflow_requantisation, priority=1)
        self.task = Task.objects.create(filter='b',sas_id=54321, status='stored', workflow=workflow_requantisation, priority=1)


        # Login the test user
        self.client = Client()
        self.client.login(username='testuser', password='testpassword')

    def test_change_priority(self):
        # Set up the URL for the view
        url = reverse('task-change-priority', kwargs={'pk': self.task.pk, 'priority_change': 2, 'page': 1})

        # Call the view using the client
        response = self.client.get(url)

        # Check if the task priority has been updated
        updated_task = Task.objects.get(pk=self.task.pk)
        self.assertEqual(updated_task.priority, 3)


    def test_change_priority_negative_priority(self):
        # Set up the URL for the view with a negative priority_change value
        url = reverse('task-change-priority', kwargs={'pk': self.task.pk, 'priority_change': -2, 'page': 0})

        # Call the view using the client
        response = self.client.get(url)

        # Check if the task priority is set to 0 when priority becomes negative
        updated_task = Task.objects.get(pk=self.task.pk)
        self.assertEqual(updated_task.priority, 0)

    def test_change_priority_sasid(self):
        # Set up the URL for the view
        url = reverse('task-change-priority-sasid', kwargs={'pk': self.task.pk, 'priority_change': 2, 'page': 1})

        # Call the view using the client
        response = self.client.get(url)

        # Check if the task priority has been updated
        updated_task = Task.objects.get(pk=self.task.pk)
        self.assertEqual(updated_task.priority, 3)