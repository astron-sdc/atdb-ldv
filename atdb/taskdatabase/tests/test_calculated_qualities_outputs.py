no_summary = {
    "quality": {
        "details": {},
        "observing-conditions": "N/A",
        "sensitivity": "N/A",
        "uv-coverage": "N/A"
    },
}

default_summary_flavour_with_rfi_percent_zero_1 = {
    "quality": {
        "details": {},
        "observing-conditions": "N/A",
        "sensitivity": "N/A",
        "summary": {
            "L526107_summaryIS.tar": {
                "added": [],
                "deleted": [],
                "input_name": "L526107_summaryIS.tar",
                "input_size": 495749120,
                "input_size_str": "472.78 MB",
                "output_name": "L526107_summaryIS.tar",
                "output_size": 283791360,
                "output_size_str": "270.64 MB",
                "rfi_percent": 0,
                "size_ratio": 0.5724495486749427
            }
        },
        "uv-coverage": "N/A"
    },
}

default_summary_flavour_with_rfi_1 = {
    "quality": {
        "details": {},
        "observing-conditions": "N/A",
        "sensitivity": "N/A",
        "summary": {
            "L526107_SAP002_B073_P000_bf.tar": {
                "added": [
                    "stokes/SAP2/BEAM73/L526105_SAP2_BEAM73_2bit.fits",
                    "stokes/SAP2/BEAM73/L526105_SAP2_BEAM73_2bit_ldv_psrfits_requantisation.log"
                ],
                "deleted": [
                    "stokes/SAP2/BEAM73/L526105_SAP2_BEAM73.fits"
                ],
                "input_name": "L526107_SAP002_B073_P000_bf.tar",
                "input_size": 20353853440,
                "input_size_str": "18.96 GB",
                "output_name": "L526107_SAP002_B073_P000_bf.tar",
                "output_size": 6024990720,
                "output_size_str": "5.61 GB",
                "rfi_percent": 11.167,
                "size_ratio": 0.2960122876860019
            }
        },
        "uv-coverage": "N/A"
    },
}

default_summary_flavour_with_rfi_2 = {
    "quality": {
        "details": {},
        "observing-conditions": "N/A",
        "sensitivity": "N/A",
        "summary": {
            "L526107_SAP002_B073_P000_bf.tar": {
                "added": [
                    "stokes/SAP2/BEAM73/L526105_SAP2_BEAM73_2bit.fits",
                    "stokes/SAP2/BEAM73/L526105_SAP2_BEAM73_2bit_ldv_psrfits_requantisation.log"
                ],
                "deleted": [
                    "stokes/SAP2/BEAM73/L526105_SAP2_BEAM73.fits"
                ],
                "input_name": "L526107_SAP002_B073_P000_bf.tar",
                "input_size": 20353853440,
                "input_size_str": "18.96 GB",
                "output_name": "L526107_SAP002_B073_P000_bf.tar",
                "output_size": 6024990720,
                "output_size_str": "5.61 GB",
                "rfi_percent": 22.167,
                "size_ratio": 0.2960122876860019
            }
        },
        "uv-coverage": "N/A"
    },
}

default_summary_flavour_with_rfi_3 = {
    "quality": {
        "details": {},
        "observing-conditions": "N/A",
        "sensitivity": "N/A",
        "summary": {
            "L526107_SAP002_B072_P000_bf.tar": {
                "added": [
                    "stokes/SAP2/BEAM72/L526105_SAP2_BEAM72_2bit.fits",
                    "stokes/SAP2/BEAM72/L526105_SAP2_BEAM72_2bit_ldv_psrfits_requantisation.log"
                ],
                "deleted": [
                    "stokes/SAP2/BEAM72/L526105_SAP2_BEAM72.fits"
                ],
                "input_name": "L526107_SAP002_B072_P000_bf.tar",
                "input_size": 20353843200,
                "input_size_str": "18.96 GB",
                "output_name": "L526107_SAP002_B072_P000_bf.tar",
                "output_size": 6024980480,
                "output_size_str": "5.61 GB",
                "rfi_percent": 31.921,
                "size_ratio": 0.2960119335104242
            }
        },
        "uv-coverage": "N/A"
    },

}

default_summary_flavour_with_rfi_4 = {
    "quality": {
        "details": {},
        "observing-conditions": "N/A",
        "sensitivity": "N/A",
        "summary": {
            "L526107_SAP002_B070_P000_bf.tar": {
                "added": [
                    "stokes/SAP2/BEAM70/L526105_SAP2_BEAM70_2bit.fits",
                    "stokes/SAP2/BEAM70/L526105_SAP2_BEAM70_2bit_ldv_psrfits_requantisation.log"
                ],
                "deleted": [
                    "stokes/SAP2/BEAM70/L526105_SAP2_BEAM70.fits"
                ],
                "input_name": "L526107_SAP002_B070_P000_bf.tar",
                "input_size": 20353525760,
                "input_size_str": "18.96 GB",
                "output_name": "L526107_SAP002_B070_P000_bf.tar",
                "output_size": 6024755200,
                "output_size_str": "5.61 GB",
                "rfi_percent": 52.164,
                "size_ratio": 0.2960054818531843
            }
        },
        "uv-coverage": "N/A"
    },
}

default_summary_flavour_without_rfi_1 = {
    "quality": {
        "details": {},
        "observing-conditions": "N/A",
        "sensitivity": "N/A",
        "summary": {
            "L526107_SAP002_B072_P000_bf.tar": {
                "added": [
                    "stokes/SAP2/BEAM72/L526105_SAP2_BEAM72_2bit.fits",
                    "stokes/SAP2/BEAM72/L526105_SAP2_BEAM72_2bit_ldv_psrfits_requantisation.log"
                ],
                "deleted": [
                    "stokes/SAP2/BEAM72/L526105_SAP2_BEAM72.fits"
                ],
                "input_name": "L526107_SAP002_B072_P000_bf.tar",
                "input_size": 20353843200,
                "input_size_str": "18.96 GB",
                "output_name": "L526107_SAP002_B072_P000_bf.tar",
                "output_size": 6024980480,
                "output_size_str": "5.61 GB",
                "size_ratio": 0.2960119335104242
            }
        },
        "uv-coverage": "N/A"
    },

}

default_summary_flavour_without_rfi_2 = {
    "quality": {
        "details": {},
        "observing-conditions": "N/A",
        "sensitivity": "N/A",
        "summary": {
            "L526107_SAP002_B070_P000_bf.tar": {
                "added": [
                    "stokes/SAP2/BEAM70/L526105_SAP2_BEAM70_2bit.fits",
                    "stokes/SAP2/BEAM70/L526105_SAP2_BEAM70_2bit_ldv_psrfits_requantisation.log"
                ],
                "deleted": [
                    "stokes/SAP2/BEAM70/L526105_SAP2_BEAM70.fits"
                ],
                "input_name": "L526107_SAP002_B070_P000_bf.tar",
                "input_size": 20353525760,
                "input_size_str": "18.96 GB",
                "output_name": "L526107_SAP002_B070_P000_bf.tar",
                "output_size": 6024755200,
                "output_size_str": "5.61 GB",
                "size_ratio": 0.2960054818531843
            }
        },
        "uv-coverage": "N/A"
    },
}

imaging_compression_summary_flavor_with_rfi_1 = {
    "quality": {
        "summary": {
            "details": {
                "DStDev": {
                    "CS001": 15372035.9671943,
                    "CS002": 14858111.10350275,
                    "CS003": 10147611.046423668,
                    "CS004": 18980165.035334244,
                    "CS005": 9209186.769605417,
                    "CS006": 15544054.561004732,
                    "CS007": 15737019.571027506,
                    "CS011": 14245094.062691605,
                    "CS013": 10705936.655357886,
                    "CS017": 14099126.5756219,
                    "CS021": 13172990.03150767,
                    "CS024": 14696724.018343825,
                    "CS026": 18501377.981954917,
                    "CS028": 14326771.584380083,
                    "CS030": 16033335.687642261,
                    "CS031": 20901704.500670947,
                    "CS032": 18795952.493532542,
                    "CS101": 67594399.69123329,
                    "CS103": 14555006.230974862,
                    "CS201": 11491082.207871344,
                    "CS301": 40468265.70497692,
                    "CS302": 17781663.389931183,
                    "CS401": 19556709.0685369,
                    "CS501": 27261643.796409346,
                    "DE601": 17777132.55854045,
                    "DE602": 19748901.556048356,
                    "DE603": 26819783.45521549,
                    "DE604": 14385497.046839358,
                    "DE605": 12729490.454671673,
                    "DE609": 11548756.244492985,
                    "FR606": 13169448.64903064,
                    "IE613": 13395597.406249378,
                    "LV614": 11668296.995990513,
                    "PL610": 14960883.74047425,
                    "PL611": 17196733.845408365,
                    "PL612": 10283464.55136512,
                    "RS106": 12128338.820957774,
                    "RS205": 42916272.60510826,
                    "RS208": 9365468.17970955,
                    "RS210": 47000312.251054004,
                    "RS305": 15538054.639135055,
                    "RS306": 14206058.107420009,
                    "RS307": 14757246.239034232,
                    "RS310": 14171170.538164835,
                    "RS406": 15226166.937623236,
                    "RS407": 14530681.276822567,
                    "RS409": 14725610.814889988,
                    "RS503": 11508097.846546676,
                    "RS508": 28514459.964421105,
                    "RS509": 19256534.542812303,
                    "SE607": 30430197.90790976,
                    "UK608": 22423233.01862699
                },
                "target": [
                    "3C295"
                ],
                "antennas": [
                    "CS001HBA0",
                    "CS001HBA1",
                    "CS002HBA0",
                    "CS002HBA1",
                    "CS003HBA0",
                    "CS003HBA1",
                    "CS004HBA0",
                    "CS004HBA1",
                    "CS005HBA0",
                    "CS005HBA1",
                    "CS006HBA0",
                    "CS006HBA1",
                    "CS007HBA0",
                    "CS007HBA1",
                    "CS011HBA0",
                    "CS011HBA1",
                    "CS013HBA0",
                    "CS013HBA1",
                    "CS017HBA0",
                    "CS017HBA1",
                    "CS021HBA0",
                    "CS021HBA1",
                    "CS024HBA0",
                    "CS024HBA1",
                    "CS026HBA0",
                    "CS026HBA1",
                    "CS028HBA0",
                    "CS028HBA1",
                    "CS030HBA0",
                    "CS030HBA1",
                    "CS031HBA0",
                    "CS031HBA1",
                    "CS032HBA0",
                    "CS032HBA1",
                    "CS101HBA0",
                    "CS101HBA1",
                    "CS103HBA0",
                    "CS103HBA1",
                    "CS201HBA0",
                    "CS201HBA1",
                    "CS301HBA0",
                    "CS301HBA1",
                    "CS302HBA0",
                    "CS302HBA1",
                    "CS401HBA0",
                    "CS401HBA1",
                    "CS501HBA0",
                    "CS501HBA1",
                    "RS106HBA",
                    "RS205HBA",
                    "RS208HBA",
                    "RS210HBA",
                    "RS305HBA",
                    "RS306HBA",
                    "RS307HBA",
                    "RS310HBA",
                    "RS406HBA",
                    "RS407HBA",
                    "RS409HBA",
                    "RS503HBA",
                    "RS508HBA",
                    "RS509HBA",
                    "DE601HBA",
                    "DE602HBA",
                    "DE603HBA",
                    "DE604HBA",
                    "DE605HBA",
                    "FR606HBA",
                    "SE607HBA",
                    "UK608HBA",
                    "DE609HBA",
                    "PL610HBA",
                    "PL611HBA",
                    "PL612HBA",
                    "IE613HBA"
                ],
                "pointing": {
                    "Sun": 98.62325727494583,
                    "CasA": 63.8887478639975,
                    "CygA": 57.33860706164162,
                    "HerA": 57.53230892059052,
                    "Moon": 82.10124202600636,
                    "TauA": 93.60818880478796,
                    "VirA": 44.64319497995252,
                    "Jupiter": 65.56149628509407,
                    "elevation_fraction": 1
                },
                "rfi_percent": 1.7186448587105623,
                "antenna_configuration": "FULL",
                "antennas_not_available": [
                    "LV614"
                ]
            },
            "applied_fixes": [],
            "rfi_perc_total": "good",
            "elevation_score": "good",
            "sun_interference": "good",
            "unfixable_issues": [],
            "moon_interference": "good",
            "jupiter_interference": "good",
            "degree_incompleteness_array": [],
            "array_missing_important_pairs_is": "good",
            "array_missing_important_pairs_dutch": "good",
            "aggregated_array_data_losses_percentage": "poor",
            "array_high_data_loss_on_is_important_pair": "good",
            "array_high_data_loss_on_dutch_important_pair": "good"
        }
    },
}


imaging_compression_aggregation_task = {
    "quality": {
        "summary": {
            "details": {
                "DStDev": {
                    "CS001": 15372035.9671943,
                    "CS002": 14858111.10350275,
                    "CS003": 10147611.046423668,
                    "CS004": 18980165.035334244,
                    "CS005": 9209186.769605417,
                    "CS006": 15544054.561004732,
                    "CS007": 15737019.571027506,
                    "CS011": 14245094.062691605,
                    "CS013": 10705936.655357886,
                    "CS017": 14099126.5756219,
                    "CS021": 13172990.03150767,
                    "CS024": 14696724.018343825,
                    "CS026": 18501377.981954917,
                    "CS028": 14326771.584380083,
                    "CS030": 16033335.687642261,
                    "CS031": 20901704.500670947,
                    "CS032": 18795952.493532542,
                    "CS101": 67594399.69123329,
                    "CS103": 14555006.230974862,
                    "CS201": 11491082.207871344,
                    "CS301": 40468265.70497692,
                    "CS302": 17781663.389931183,
                    "CS401": 19556709.0685369,
                    "CS501": 27261643.796409346,
                    "DE601": 17777132.55854045,
                    "DE602": 19748901.556048356,
                    "DE603": 26819783.45521549,
                    "DE604": 14385497.046839358,
                    "DE605": 12729490.454671673,
                    "DE609": 11548756.244492985,
                    "FR606": 13169448.64903064,
                    "IE613": 13395597.406249378,
                    "LV614": 11668296.995990513,
                    "PL610": 14960883.74047425,
                    "PL611": 17196733.845408365,
                    "PL612": 10283464.55136512,
                    "RS106": 12128338.820957774,
                    "RS205": 42916272.60510826,
                    "RS208": 9365468.17970955,
                    "RS210": 47000312.251054004,
                    "RS305": 15538054.639135055,
                    "RS306": 14206058.107420009,
                    "RS307": 14757246.239034232,
                    "RS310": 14171170.538164835,
                    "RS406": 15226166.937623236,
                    "RS407": 14530681.276822567,
                    "RS409": 14725610.814889988,
                    "RS503": 11508097.846546676,
                    "RS508": 28514459.964421105,
                    "RS509": 19256534.542812303,
                    "SE607": 30430197.90790976,
                    "UK608": 22423233.01862699
                },
                "target": [
                    "3C295"
                ],
                "quality": "moderate",
                "median_dataloss": 1.23456789,
                "quality_indicators": "rfi_limit:0.3; median_dataloss_limit:1.5; sun_angle_limit:20; moon_angle_limit:10; jupiter_angle_limit:5",

                "antennas": [
                    "CS001HBA0",
                    "CS001HBA1",
                    "CS002HBA0",
                    "CS002HBA1",
                    "CS003HBA0",
                    "CS003HBA1",
                    "CS004HBA0",
                    "CS004HBA1",
                    "CS005HBA0",
                    "CS005HBA1",
                    "CS006HBA0",
                    "CS006HBA1",
                    "CS007HBA0",
                    "CS007HBA1",
                    "CS011HBA0",
                    "CS011HBA1",
                    "CS013HBA0",
                    "CS013HBA1",
                    "CS017HBA0",
                    "CS017HBA1",
                    "CS021HBA0",
                    "CS021HBA1",
                    "CS024HBA0",
                    "CS024HBA1",
                    "CS026HBA0",
                    "CS026HBA1",
                    "CS028HBA0",
                    "CS028HBA1",
                    "CS030HBA0",
                    "CS030HBA1",
                    "CS031HBA0",
                    "CS031HBA1",
                    "CS032HBA0",
                    "CS032HBA1",
                    "CS101HBA0",
                    "CS101HBA1",
                    "CS103HBA0",
                    "CS103HBA1",
                    "CS201HBA0",
                    "CS201HBA1",
                    "CS301HBA0",
                    "CS301HBA1",
                    "CS302HBA0",
                    "CS302HBA1",
                    "CS401HBA0",
                    "CS401HBA1",
                    "CS501HBA0",
                    "CS501HBA1",
                    "RS106HBA",
                    "RS205HBA",
                    "RS208HBA",
                    "RS210HBA",
                    "RS305HBA",
                    "RS306HBA",
                    "RS307HBA",
                    "RS310HBA",
                    "RS406HBA",
                    "RS407HBA",
                    "RS409HBA",
                    "RS503HBA",
                    "RS508HBA",
                    "RS509HBA",
                    "DE601HBA",
                    "DE602HBA",
                    "DE603HBA",
                    "DE604HBA",
                    "DE605HBA",
                    "FR606HBA",
                    "SE607HBA",
                    "UK608HBA",
                    "DE609HBA",
                    "PL610HBA",
                    "PL611HBA",
                    "PL612HBA",
                    "IE613HBA"
                ],
                "pointing": {
                    "Sun": 98.62325727494583,
                    "CasA": 63.8887478639975,
                    "CygA": 57.33860706164162,
                    "HerA": 57.53230892059052,
                    "Moon": 82.10124202600636,
                    "TauA": 93.60818880478796,
                    "VirA": 44.64319497995252,
                    "Jupiter": 65.56149628509407,
                    "elevation_fraction": 1
                },
                "rfi_percent": 1.7186448587105623,
                "antenna_configuration": "FULL",
                "antennas_not_available": [
                    "LV614"
                ]
            },
            "applied_fixes": [],
            "rfi_perc_total": "good",
            "elevation_score": "good",
            "sun_interference": "good",
            "unfixable_issues": [],
            "moon_interference": "good",
            "jupiter_interference": "good",
            "degree_incompleteness_array": [],
            "array_missing_important_pairs_is": "good",
            "array_missing_important_pairs_dutch": "good",
            "aggregated_array_data_losses_percentage": "poor",
            "array_high_data_loss_on_is_important_pair": "good",
            "array_high_data_loss_on_dutch_important_pair": "good"
        }
    },
}

link_calibrator_summary_without_rfi = {
    "quality": {
        "details": {},
        "sensitivity": "N/A",
        "uv-coverage": "N/A",
        "observing-conditions": "N/A"
    },
    "summary": {
        "size": 17464,
        "surl": "srm://srm.grid.sara.nl/pnfs/grid.sara.nl/data/lofar/ops/disk/ldv/lt10_010/689478/35011/3c48_LINC_calibrator_summary.json",
        "class": "File",
        "nameext": ".json",
        "basename": "3c48_LINC_calibrator_summary.json",
        "checksum": "sha1$531646ff527d76f4facdabf72d939bac302eaf1f",
        "location": "file:///project/ldv/Share/run/2023/6/16/1352_35011/3c48_LINC_calibrator_summary.json",
        "nameroot": "3c48_LINC_calibrator_summary"
    },
}

link_target_summary_without_rfi = {
    "quality": {
        "details": {},
        "sensitivity": "N/A",
        "uv-coverage": "N/A",
        "observing-conditions": "N/A"
    },
    "summary": {
        "size": 17464,
        "surl": "srm://srm.grid.sara.nl/pnfs/grid.sara.nl/data/lofar/ops/disk/ldv/lt10_010/689478/35011/3c48_LINC_calibrator_summary.json",
        "class": "File",
        "nameext": ".json",
        "basename": "3c48_LINC_target_summary.json",
        "checksum": "sha1$531646ff527d76f4facdabf72d939bac302eaf1f",
        "location": "file:///project/ldv/Share/run/2023/6/16/1352_35011/3c48_LINC_target_summary.json",
        "nameroot": "3c48_LINC_target_summary"
    },
}

imaging_compression_with_provided_quality_ok = {
    "quality": {
        "summary": {
            "details": {
                "DStDev": {
                    "CS001": 15372035.9671943,
                    "UK608": 22423233.01862699
                },
                "target": [
                    "3C295"
                ],
                "antennas": [
                    "CS001HBA0",
                    "IE613HBA"
                ],
                "pointing": {
                    "Sun": 98.62325727494583,
                    "CasA": 63.8887478639975,
                    "CygA": 57.33860706164162,
                    "HerA": 57.53230892059052,
                    "Moon": 82.10124202600636,
                    "TauA": 93.60818880478796,
                    "VirA": 44.64319497995252,
                    "Jupiter": 65.56149628509407,
                    "elevation_fraction": 1
                },
                "rfi_percent": 1.7186448587105623,
                "quality": "moderate",
                "antenna_configuration": "FULL",
                "antennas_not_available": [
                    "LV614"
                ]
            },
            "applied_fixes": [],
            "rfi_perc_total": "good",
            "elevation_score": "good",
            "sun_interference": "good",
            "unfixable_issues": [],
            "moon_interference": "good",
            "jupiter_interference": "good",
            "degree_incompleteness_array": [],
            "array_missing_important_pairs_is": "good",
            "array_missing_important_pairs_dutch": "good",
            "aggregated_array_data_losses_percentage": "poor",
            "array_high_data_loss_on_is_important_pair": "good",
            "array_high_data_loss_on_dutch_important_pair": "good"
        }
    },
}

imaging_compression_with_provided_quality_not_ok = {
    "quality": {
        "summary": {
            "details": {
                "DStDev": {
                    "CS001": 15372035.9671943,
                    "UK608": 22423233.01862699
                },
                "target": [
                    "3C295"
                ],
                "antennas": [
                    "CS001HBA0",
                    "IE613HBA"
                ],
                "pointing": {
                    "Sun": 98.62325727494583,
                    "CasA": 63.8887478639975,
                    "CygA": 57.33860706164162,
                    "HerA": 57.53230892059052,
                    "Moon": 82.10124202600636,
                    "TauA": 93.60818880478796,
                    "VirA": 44.64319497995252,
                    "Jupiter": 65.56149628509407,
                    "elevation_fraction": 1
                },
                "rfi_percent": 1.7186448587105623,
                "quality": "no_a_quality",
                "antenna_configuration": "FULL",
                "antennas_not_available": [
                    "LV614"
                ]
            },
            "applied_fixes": [],
            "rfi_perc_total": "good",
            "elevation_score": "good",
            "sun_interference": "good",
            "unfixable_issues": [],
            "moon_interference": "good",
            "jupiter_interference": "good",
            "degree_incompleteness_array": [],
            "array_missing_important_pairs_is": "good",
            "array_missing_important_pairs_dutch": "good",
            "aggregated_array_data_losses_percentage": "poor",
            "array_high_data_loss_on_is_important_pair": "good",
            "array_high_data_loss_on_dutch_important_pair": "good"
        }
    },
}

default_summary_flavour_multiple_files_per_task = {
    "quality": {
        "details": {},
        "observing-conditions": "N/A",
        "sensitivity": "N/A",
        "summary": {
            "L344624_SAP002_B068_P000_bf.tar": {
                "added": [
                    "stokes/SAP2/BEAM68/L344622_SAP2_BEAM68_2bit.fits",
                    "stokes/SAP2/BEAM68/L344622_SAP2_BEAM68_2bit_ldv_psrfits_requantisation.log"
                ],
                "deleted": [
                    "stokes/SAP2/BEAM68/L344622_SAP2_BEAM68.fits"
                ],
                "input_name": "L344624_SAP002_B068_P000_bf.tar",
                "input_size": 20354099200,
                "input_size_str": "18.96 GB",
                "is_summary": False,
                "output_name": "L344624_SAP002_B068_P000_bf.tar",
                "output_size": 6025144320,
                "output_size_str": "5.61 GB",
                "rfi_percent": 10.174,
                "size_ratio": 0.2960162599580924
            },
            "L344624_SAP002_B069_P000_bf.tar": {
                "added": [
                    "stokes/SAP2/BEAM69/L344622_SAP2_BEAM69_2bit.fits",
                    "stokes/SAP2/BEAM69/L344622_SAP2_BEAM69_2bit_ldv_psrfits_requantisation.log"
                ],
                "deleted": [
                    "stokes/SAP2/BEAM69/L344622_SAP2_BEAM69.fits"
                ],
                "input_name": "L344624_SAP002_B069_P000_bf.tar",
                "input_size": 20354088960,
                "input_size_str": "18.96 GB",
                "is_summary": False,
                "output_name": "L344624_SAP002_B069_P000_bf.tar",
                "output_size": 6025134080,
                "output_size_str": "5.61 GB",
                "rfi_percent": 20.203,
                "size_ratio": 0.2960159057887895
            },
            "L344624_SAP002_B070_P000_bf.tar": {
                "added": [
                    "stokes/SAP2/BEAM70/L344622_SAP2_BEAM70_2bit.fits",
                    "stokes/SAP2/BEAM70/L344622_SAP2_BEAM70_2bit_ldv_psrfits_requantisation.log"
                ],
                "deleted": [
                    "stokes/SAP2/BEAM70/L344622_SAP2_BEAM70.fits"
                ],
                "input_name": "L344624_SAP002_B070_P000_bf.tar",
                "input_size": 20354140160,
                "input_size_str": "18.96 GB",
                "is_summary": False,
                "output_name": "L344624_SAP002_B070_P000_bf.tar",
                "output_size": 6025175040,
                "output_size_str": "5.61 GB",
                "rfi_percent": 30.404,
                "size_ratio": 0.29601717353999
            },
            "L344624_SAP002_B071_P000_bf.tar": {
                "added": [
                    "stokes/SAP2/BEAM71/L344622_SAP2_BEAM71_2bit.fits",
                    "stokes/SAP2/BEAM71/L344622_SAP2_BEAM71_2bit_ldv_psrfits_requantisation.log"
                ],
                "deleted": [
                    "stokes/SAP2/BEAM71/L344622_SAP2_BEAM71.fits"
                ],
                "input_name": "L344624_SAP002_B071_P000_bf.tar",
                "input_size": 20354099200,
                "input_size_str": "18.96 GB",
                "is_summary": False,
                "output_name": "L344624_SAP002_B071_P000_bf.tar",
                "output_size": 6025134080,
                "output_size_str": "5.61 GB",
                "rfi_percent": 50.416,
                "size_ratio": 0.2960157568653296
            }
        },
        "uv-coverage": "N/A"
    },
}