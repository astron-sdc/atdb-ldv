import logging
from .common import get_summary_flavour, SummaryFlavour
logger = logging.getLogger(__name__)


def rfi_percentage_to_quality(rfi_percent, quality_treshold_moderate, quality_treshold_poor):
    quality = None
    # > 0 is intentional, by decision of SDCO
    # 0 means that this task should not be considered in the calculation, because it is probably a summary file.
    if rfi_percent > 0:
        quality = "good"
        if rfi_percent >= quality_treshold_moderate:
            quality = "moderate"
        if rfi_percent > quality_treshold_poor:
            quality = "poor"

    elif rfi_percent == 0:
        quality = "none"

    return quality


def unpack_qualities_per_task(task, qualities):
    """
    unpack the value of key and add it to qualities
    param qualities: existing list of qualities and count
    param key: can either be string  or a list
    return: updated list of qualities
    """
    try:
        key = task.calculated_qualities['per_task']

        if type(key) is list:
            for q in key:
                qualities[q] = qualities[q] + 1

        else:
            # a single string (poor, moderate, good), for backward for compatibility reasons
            qualities[key] = qualities[key] + 1
    except:
        # if anything fails, then just return the original array
        pass

    return qualities


def calculate_qualities(task, tasks_for_this_sasid, quality_thresholds):
    """"
    calculate the quality for this task, but also the quality for all the combined tasks of this sas_id
    """

    def calculate_quality_task(task):
        """
        calculate the quality of this task based on rfi_percent values
        The threshold values are read from a configuration json blob

        Using this algorithm from SDCO:
                rfi_i <= 20 % is good
                20% <= rfi_i <= 50 is moderate
                rfi_i > 50 is poor.
                except when rfi_percent	= 0
        """
        try:
            qualities_per_task = []
            summary = task.quality_json["summary"]
            summary_flavour = get_summary_flavour(task)

            if summary_flavour == SummaryFlavour.IMAGING_COMPRESSION.value:
                quality_per_file = None

                # override, if quality is already calculated by the workflow itself, then do not recalculate
                try:
                    quality_from_summary = summary['details']['quality']
                    if quality_from_summary in ['poor', 'moderate', 'good']:
                        quality_per_file = quality_from_summary
                except:
                    # no quality key found, continue with rfi_percent
                    pass

                # this workflow has only 1 rfi_percent per task
                if not quality_per_file:
                    rfi_percent = float(summary['details']['rfi_percent'])
                    quality_per_file = rfi_percentage_to_quality(rfi_percent, quality_thresholds['moderate'], quality_thresholds['poor'])

                # needs to return an array of qualities, because other workflows may have multiple files per task
                qualities_per_task.append(quality_per_file)


            if summary_flavour == SummaryFlavour.DEFAULT.value:
                # summary is a dict, with (unknown) filenames as a key, look for 'rfi_percent' in them

                for key in summary:
                    record = summary[key]
                    rfi_percent = float(record['rfi_percent'])

                    # these workflows can have multiple rfi_percent's per task
                    quality_per_file = rfi_percentage_to_quality(rfi_percent, quality_thresholds['moderate'], quality_thresholds['poor'])
                    qualities_per_task.append(quality_per_file)

            return qualities_per_task
            #return rfi_percentage_to_quality(rfi_percent, quality_thresholds['moderate'], quality_thresholds['poor'])

        except Exception as error:
            # when rfi_percentage is missing, then the quality cannot be calculated.
            # Just continue without it
            pass


    def calculate_quality_sasid(unsaved_task, tasks_for_this_sasid):
        """
        calculate the overall quality per sas_id, based on other tasks with the same sas_id

        For the imaging compression pipeline (IMAGING_COMPRESSION flavour) this value is read from the aggregation task
        For other pipelines it is calculated based on threshold values stored in ATDB

        The threshold values are read from a configuration json blob

        Using this algorithm from SDCO:
             if more then 90 % of all files have a good quality then the dataset has good condition.
             If more then 50 % of all files have a poor quality then the dataset is poor
             otherwise is moderate.
        """

        summary_flavour = get_summary_flavour(unsaved_task)

        if summary_flavour == SummaryFlavour.IMAGING_COMPRESSION.value:
            # because the quality is read from the aggregation task, only the aggregation task needs to be checked
            if unsaved_task.task_type == 'aggregation':
                summary = unsaved_task.quality_json["summary"]
                quality_from_summary = summary['details']['quality']
                return quality_from_summary
            else:
                return None

        if summary_flavour == SummaryFlavour.DEFAULT.value:
            try:
                # gather the results of all the calculated_quality values for this sas_id
                qualities = {'poor': 0, 'moderate': 0, 'good': 0}

                # also add the currently unsaved task to the list for the quality calculation per sas_id
                tasks = list(tasks_for_this_sasid)
                tasks.append(unsaved_task)

                for task in tasks:

                    # skip 'suspended' and 'discarded' tasks
                    if task.status in ['suspended', 'discarded']:
                        continue

                    # because this all happens in the overridden 'Task.save', the actual saving has not yet occurred
                    # So use the calculated quality from the unsaved task instead.
                    if task.id == unsaved_task.id:
                        t = unsaved_task
                    else:
                        t = task

                    try:
                        qualities = unpack_qualities_per_task(t, qualities)

                    except:
                        # ignore the tasks that have no calculated quality.
                        pass


                total = qualities['poor'] + qualities['moderate'] + qualities['good']
                quality_sasid = None
                if total > 0:
                    percentage_poor = (qualities['poor'] / total) * 100
                    percentage_good = (qualities['good'] / total) * 100
                    quality_sasid = "moderate"

                    if percentage_poor >= quality_thresholds['overall_poor']:
                        quality_sasid = 'poor'

                    if percentage_good >= quality_thresholds['overall_good']:
                        quality_sasid = 'good'

                return quality_sasid

            except Exception as error:
                logger.error(error)


    # --- main function body ---
    # calculate the quality for this task
    qualities = {}
    try:
        calculated_qualities_per_task = calculate_quality_task(task)

        # store the result in task.calculated_qualities (not yet saved in the database)
        qualities = task.calculated_qualities
        if not qualities:
            qualities = {}

        qualities['per_task'] = calculated_qualities_per_task
        task.calculated_qualities = qualities

        # update the overall quality of all tasks for this sas_id
        calculated_quality_sasid = calculate_quality_sasid(task, tasks_for_this_sasid)

        if calculated_quality_sasid:
            # store the result in the activity, and save it
            task.activity.calculated_quality = calculated_quality_sasid
            task.activity.save()

    except Exception as error:
        logger.error(error)

    return qualities

