import logging;
import datetime
from django.db.models.signals import pre_save, post_save
from django.core.signals import request_started, request_finished
from django.contrib.auth.models import User
from django.dispatch import receiver
from django.contrib.contenttypes.models import ContentType
from django.utils import timezone
from taskdatabase.models import Task, Workflow, LogEntry, Status
from .activities_handler import update_activity


"""
Signals sent from different parts of the backend are centrally defined and handled here.
"""

logger = logging.getLogger(__name__)

def add_logentry(task, step, status, description, service='ATDB'):
    """
    add log entry
    usually this functionality is called by external services directly using the REST API to report to ATDB
    but there are also 'internal services', like input_validation, that can report to the user this way.
    """
    logentry = LogEntry(
        task=task,
        service=service,
        step_name=step,
        status=status,
        description=description,
        timestamp=timezone.now())
    logentry.save()


#--- Task signals-------------

@receiver(pre_save, sender=Task)
def pre_save_task_handler(sender, **kwargs):
    #logger.info("SIGNAL : pre_save Task(" + str(kwargs.get('instance')) + ")")
    handle_pre_save(sender, **kwargs)

def handle_pre_save(sender, **kwargs):
    """
    pre_save handler. Mainly to check status changes and dispatch jobs in needed.
    :param (in) sender: The model class that sends the trigger
    :param (in) kwargs: The instance of the object that sends the trigger.
    """
    #logger.info("handle_pre_save(" + str(kwargs.get('instance')) + ")")
    task = kwargs.get('instance')

    # IF this object does not exist yet, then abort, and let it first be handled by handle_post_save (get get a id).
    if task.id==None:
        return None

    # handle status change
    status = str(task.status)
    new_status = str(task.new_status)

    if (new_status is not None) and (status!=new_status):

        # set the new status
        task.status = new_status

        # add the new status to the status history
        myStatus = Status(name=new_status, task=task)
        myStatus.save()

    disconnect_signals()
    task.save()
    connect_signals()


@receiver(post_save, sender=Task)
def post_save_task_handler(sender, **kwargs):
    handle_post_save(sender, **kwargs)

def handle_post_save(sender, **kwargs):
    """
    post_save handler. Update activities and do some calculations
    :param (in) sender: The model class that sends the trigger
    :param (in) kwargs: The instance of the object that sends the trigger.
    """
    task = kwargs.get('instance')

    update_activity(task)

    if task.environment:
        step = task.environment.split('::')[0]
        description = task.environment.split('::')[1]
        add_logentry(task,step,task.status,description)

        # clear the 'cache'
        task.environment=''
        disconnect_signals()
        task.save()
        connect_signals()

def connect_signals():
    #logger.info("connect_signals")
    pre_save.connect(pre_save_task_handler, sender=Task)
    post_save.connect(post_save_task_handler, sender=Task)

def disconnect_signals():
    #logger.info("disconnect_signals")
    pre_save.disconnect(pre_save_task_handler, sender=Task)
    post_save.disconnect(post_save_task_handler, sender=Task)