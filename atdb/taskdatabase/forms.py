from django import forms
from django.forms import Textarea
from .models import Task
import json
class QualityAnnotationForm(forms.Form):
      annotation = forms.CharField(label='', widget=forms.Textarea(attrs={'rows': 12,'cols': 85}),required=False,max_length=1000)
      return_to_page = forms.IntegerField(widget=forms.HiddenInput)


class DiscardAnnotationForm(forms.Form):
    annotation = forms.CharField(label='', widget=forms.Textarea(attrs={'rows': 3, 'cols': 85}), required=False,
                                 max_length=250)
    return_to_page = forms.IntegerField(widget=forms.HiddenInput)


class TaskBulkUpdateForm(forms.ModelForm):
    # Checkbox fields for each field to decide which ones to update
    filter_update = forms.BooleanField(required=False, label="Update filter")
    #new_status_update = forms.BooleanField(required=False, label="Update new_status")
    quality_update = forms.BooleanField(required=False, label="Update quality")
    resume_update = forms.BooleanField(required=False, label="Update resume")
    priority_update = forms.BooleanField(required=False, label="Update priority")
    purge_policy_update = forms.BooleanField(required=False, label="Update purge_policy")
    #cleanup_policy_update = forms.BooleanField(required=False, label="Update cleanup_policy")
    project_update = forms.BooleanField(required=False, label="Update project")
    sas_id_update = forms.BooleanField(required=False, label="Update SAS_ID")
    #metrics_update = forms.BooleanField(required=False, label="Update metrics")
    remarks_update = forms.BooleanField(required=False, label="Update remarks")
    meta_scheduling_update = forms.BooleanField(required=False, label="Update meta_scheduling")
    service_filter_update = forms.BooleanField(required=False, label="Update service_filter")
    #workflow_update = forms.BooleanField(required=False, label="Update workflow")

    class Meta:
        model = Task
        fields = [
            'filter', 'priority','quality',
            'project', 'sas_id','resume','purge_policy',
            'remarks', 'meta_scheduling',
            'service_filter'
        ]

    def __init__(self, *args, task_list=None, **kwargs):
        super().__init__(*args, **kwargs)

        if task_list:
            # Set initial values based on task_list
            for field in self.Meta.fields:
                # Gather all values for this field across tasks in the list
                values = [getattr(task, field) for task in task_list]

                serialized_values = {json.dumps(val, sort_keys=True) for val in values}

                # Check if all values are the same
                if len(set(serialized_values)) == 1:
                    self.fields[field].initial = values[0]
                else:
                    self.fields[field].initial = None
                    self.fields[field].help_text = "values for this field differ per task."

        # Customize the widget for JSONField to reduce size
        self.fields['meta_scheduling'].widget = Textarea(attrs={
            'rows': 4,  # Set the number of visible rows
            'cols': 80  # Set the number of visible columns
        })

        self.fields['remarks'].widget = Textarea(attrs={
            'rows': 4,  # Set the number of visible rows
            'cols': 80  # Set the number of visible columns
        })