from django.urls import include, path
from django.contrib.auth import views as auth_views
from rest_framework.authtoken import views as rest_auth_views

from . import views

urlpatterns = [

    # authentication
    path('accounts/', include('django.contrib.auth.urls')),

    # specialization of the above, with more control
    path('login/', auth_views.LoginView.as_view(template_name='registration/login.html')),

    # --- GUI ---
    path('', views.IndexView.as_view(), name='index'),
    path('postprocessing-tasks', views.PostProcessingTasksView.as_view(), name='postprocessing-tasks'),
    path('quality', views.ShowQualityPage.as_view(), name='quality'),
    path('validation', views.ShowValidationPage.as_view(), name='validation'),
    path('failures', views.ShowFailuresPage.as_view(), name='failures'),
    path('discarded', views.ShowDiscardedPage.as_view(), name='discarded'),
    path('ingest', views.ShowIngestQPage.as_view(), name='ingest'),
    path('finished', views.ShowFinishedPage.as_view(), name='finished'),


    path('task_details/<int:id>/<page>', views.TaskDetails, name='task-details'),
    path('task_details/', views.TaskDetails, name='task-details'),
    path('task_quality/<int:id>/<page>', views.ShowTaskQuality, name='task-quality'),
    path('task_quality/', views.ShowTaskQuality, name='task-quality'),
    path('annotate_quality_taskid/<int:id>/<page>', views.AnnotateQualityTaskId, name='annotate-quality-taskid'),
    path('annotate_quality_taskid/<int:id>', views.AnnotateQualityTaskId, name='annotate-quality-taskid'),
    path('annotate_quality_taskid/', views.AnnotateQualityTaskId, name='annotate-quality-taskid'),
    path('annotate_quality_sasid/<int:id>', views.AnnotateQualitySasId, name='annotate-quality-sasid'),
    path('annotate_quality_sasid/<int:id>/<page>', views.AnnotateQualitySasId, name='annotate-quality-sasid'),
    path('show_inspectionplots/<int:id>/<page>', views.ShowInspectionPlots, name='inspection-plots'),
    path('clear_annotations_sasid/<int:id>', views.ClearAnnotationsSasID, name='clear-annotations-sasid'),
    path('clear_annotations_sasid/<int:id>/<page>', views.ClearAnnotationsSasID, name='clear-annotations-sasid'),
    path('show_inspectionplots_sasid/<int:id>/<expand_image>', views.ShowInspectionPlotsSasId, name='inspection-plots-sasid'),

    path('show_summary/<int:id>/<page>', views.ShowSummarySasId, name='summary'),

    path('show-inputs/<int:id>/', views.ShowInputs, name='show-inputs'),
    path('show-outputs/<int:id>/', views.ShowOutputs, name='show-outputs'),
    path('show-metrics/<int:id>/', views.ShowMetrics, name='show-metrics'),
    path('dashboard/<selection>', views.ShowDashboard, name='dashboard'),

    path('workflow_details/<id>/', views.WorkflowDetails, name='workflow-details'),
    path('query/', views.QueryView.as_view(), name='query'),
    #path('query2/<query_params>', views.QueryView.as_view(), name='query2'),

    # path('monitoring/', views.MonitoringView.as_view(), name='monitoring'),
    path('monitoring/', views.ShowMonitoring, name='monitoring'),
    path('diagram/', views.DiagramView.as_view(), name='diagram'),
    path('config/', views.ShowConfig, name='config'),
    path('create_status_graph/', views.CreateStatusGraph, name='create_status_graph'),
    path('create_logentry_graph/', views.CreateLogEntryGraph, name='create_logentry_graph'),
    # --- REST API ---

    path('obtain-auth-token/', rest_auth_views.obtain_auth_token),

    path('tasks/', views.TaskListViewAPI.as_view(), name='tasks-api'),
    path('tasks/<int:pk>/', views.TaskDetailsViewAPI.as_view(), name='task-detail-view-api'),
    path('tasks-fast/', views.TaskListViewAPIFast.as_view(), name='tasks-api-fast'),
    path('tasks-fast/<int:pk>/', views.TaskDetailsViewAPIFast.as_view(), name='task-detail-view-api-fast'),
    path('postprocessing-tasks/', views.PostProcessingTaskListViewAPI.as_view(), name='postprocessing-tasks-api'),
    path('all-tasks/', views.AllTaskListViewAPI.as_view(), name='all-tasks-api'),

    path('activities/', views.ActivityListViewAPI.as_view(), name='activities-api'),
    path('activities/<int:pk>/', views.ActivityDetailsViewAPI.as_view(), name='activity-detail-view-api'),

    path('workflows/', views.WorkflowListViewAPI.as_view(), name='workflows-api'),
    path('workflows/<int:pk>/', views.WorkflowDetailsViewAPI.as_view(), name='workflow-detail-view-api'),

    path('logentries/', views.LogEntryListViewAPI.as_view()),
    path('logentries/<int:pk>/', views.LogEntryDetailsViewAPI.as_view(), name='logentry-detail-view-api'),

    path('configuration/', views.ConfigurationListViewAPI.as_view()),
    path('configuration/<int:pk>/', views.ConfigurationDetailsViewAPI.as_view(), name='configuration-detail-view-api'),

    path('jobs/', views.JobListViewAPI.as_view(), name='job-list-view-api'),
    path('jobs/<int:pk>/', views.JobDetailsViewAPI.as_view(), name='job-detail-view-api'),

    path('postprocessing/', views.PostProcessingRuleListViewAPI.as_view()),
    path('postprocessing/<int:pk>/', views.PostProcessingRuleDetailsViewAPI.as_view(), name='postprocessing-detail-view-api'),

    path('monitor/', views.MonitorListViewAPI.as_view(),name='monitor-list-view-api'),
    path('monitor/<int:pk>/', views.MonitorDetailsViewAPI.as_view(),name='monitor-detail-view-api'),
    path('latest_monitor/', views.LatestMonitorListViewAPI.as_view(),name='latest-monitor-detail-view-api'),
    path('latest_monitor/<int:pk>/', views.LatestMonitorDetailsViewAPI.as_view(), name='latest-monitor-detail-view-api'),

    path('monitor/clear_inactive_services/', views.ClearInactiveServices, name='clear-inactive-services'),
    # path('monitoring/service_hold_resume/<int:pk>/<enabled>', views.ServiceHoldResume, name='service-hold-resume'),
    path('monitoring/service_hold_resume/<name>/<hostname>/<enabled>', views.ServiceHoldResume, name='service-hold-resume'),

    # --- custom requests ---
    # /atdb/get_size?status__in=defined,staged
    path('tasks/get_size/', views.GetSizeView.as_view(), name='get-size-view'),
    # /atdb/get_min_start_and_max_end_time?sas_id=65005
    path('get_min_start_and_max_end_time/', views.GetMinMaxTimeView.as_view(), name='get-min-start-and-max-end-time-view'),
    # /atdb/get_unique_values_for_key/<aggregation_key>/
    path('get_unique_values_for_key/<str:aggregation_key>/', views.GetUniqueValuesForKey.as_view(),
         name='get-unique-values-for-key-view'),

    # ancillary dataproducts endpoints (retrieved by archiver service for copy to LTA dcache).
    # /atdb/get_summary/606942/html
    path('get_summary/<sas_id>/<format>', views.GetSummary, name='get-summary'), # format can be 'json', 'html' or 'pdf'

    # --- controller resources ---
    path('tasks/<int:pk>/setstatus/<new_status>/<page>', views.TaskSetStatus, name='task-setstatus-view'),
    path('tasks/<int:pk>/setstatus/<new_status>', views.TaskSetStatus, name='task-details-setstatus'),

    path('activities/<int:pk>/setstatus/<new_task_status>/<new_activity_status>/<page>', views.RetryAggregation, name='retry-aggregation'),
    path('activities/<int:pk>/setstatus/<new_task_status>/<new_activity_status>', views.RetryAggregation, name='retry-aggregation'),

    path('tasks/<int:pk>/validate-sasid/<quality>/<new_status>/<page>', views.TaskValidateSasId, name='task-validate-sasid'),
    path('tasks/<int:pk>/validate-task/<quality>/<new_status>/<page>', views.TaskValidateTask, name='task-validate-task'),
    path('tasks/<int:pk>/retry/<new_status>/<page>', views.TaskRetry, name='task-retry-view'),
    path('tasks/<int:pk>/discard/<new_status>/<page>', views.TaskDiscard, name='task-discard-view'),
    path('tasks/<int:pk>/discard_sasid/<new_status>/<page>', views.TaskDiscardSasId, name='task-discard-view-sasid'),

    path('tasks/<int:pk>/change_priority/<priority_change>/<page>', views.ChangePriority, name='task-change-priority'),
    path('tasks/<int:pk>/change_priority/<priority_change>', views.ChangePriority, name='task-change-priority'),
    path('tasks/<int:pk>/change_priority_sasid/<priority_change>/<page>', views.ChangePrioritySasID, name='task-change-priority-sasid'),

    path('tasks/sort-tasks/<sort>/<redirect_to_page>', views.SortTasks, name='sort-tasks'),
    path('tasks/set_filter/<filter>/<redirect_to_page>', views.TaskSetFilter, name='task-set-filter'),
    path('tasks/set_ingest_filter/<filter>', views.TaskSetIngestFilter, name='task-set-ingest-filter'),
    path('tasks/set_active_filter/<redirect_to_page>', views.TaskSetActiveFilter, name='task-set-active-filter'),
    path('tasks/task-set-onhold-filter/<onhold>/<redirect_to_page>', views.TaskSetOnHoldFilter, name='task-set-onhold-filter'),
    path('tasks/clear_filter/<redirect_to_page>', views.TaskClearFilter, name='clear-filter'),

    path('tasks/<int:pk>/set_status/<new_status>/<query_params>', views.TaskSetStatusTables2, name = 'task-setstatus'),
    path('tasks/set_status_multi/<new_status>/<query_params>', views.TaskMultiStatus, name='task-multi-setstatus'),
    path('tasks/set_multi_hold/<onhold>/<query_params>', views.TaskMultiHold, name='task-multi-hold'),
    path('tasks/set_multi_purge/<purge_policy>/<query_params>', views.TaskMultiPurge, name='task-multi-purge'),
    path('tasks/task-multi-edit/<query_params>', views.TaskMultiEdit, name='task-multi-edit'),

    path('tasks/<int:pk>/hold/<hold_it>/<page>', views.Hold, name='task-hold-resume'),
    path('tasks/<int:pk>/hold/<hold_it>', views.Hold, name='task-hold-resume'),
    path('tasks/<int:pk>/query-hold/<hold_it>/<query_params>', views.HoldQuery, name='query-hold-resume'),
    path('tasks/<int:pk>/hold/<hold_it>/<page>', views.Hold, name='service-hold-resume'),
    path('tasks/<int:pk>/query-purge/<purge_policy>/<query_params>', views.PurgeQuery, name='query-purge'),

    #some migration and repair endpoints
    path('tasks/repair/associate-activities/', views.AssociateActivities, name='associate-activities'),
    path('tasks/repair/reassociate-activity/<task_id>', views.ReassociateActivity, name='reassociate-activity'),
    path('tasks/repair/update-activity/<sas_id>', views.UpdateActivitySasId, name='update-activity-sasid'),
    path('tasks/repair/update-summary-flag/<task_id>', views.UpdateSummaryFlag, name='update-summary-flag'),
]
