# tables.py
from django.utils.html import format_html
import django_tables2 as tables
from .models import Task

# render the StatusColumn based on the style.css
class StatusColumn(tables.Column):
    attrs = {
        "td": {
            "class": lambda record: record.status
        },
        "tr": {
            "class": lambda record: record.status
        }
    }
    def render(self, record):
        return "{}".format(record.status)


class TypeColumn(tables.BooleanColumn):
    attrs = {
        "td": {
            "class": lambda record: record.task_type
        },
        "tr": {
            "class": lambda record: record.task_type
        }
    }
    def render(self, record):
        return "{}".format(record.task_type)

# render the ResumeColumn based on the style.css
class ResumeColumn(tables.BooleanColumn):
    def render(self, record):
        return record.resume

class WorkflowColumn(tables.BooleanColumn):
    def render(self, record):
        return record.workflow.id

class ServiceFilterColumn(tables.BooleanColumn):
    def render(self, record):
        return record.service_filter

class PurgeColumn(tables.BooleanColumn):
    def render(self, record):
        return record.purge_policy

class TaskTable(tables.Table):

    class Meta:
        model = Task
        template_name = "django_tables2/bootstrap4.html"
        fields = ("id", "task_type","WF","filter","service_filter","priority","status","quality","project","sas_id","resume","purge","actions","buttons")

    # columns that need specific rendering
    #orkflowColumn()
    WF = WorkflowColumn()
    status = StatusColumn()
    purge = PurgeColumn()

    actions = tables.TemplateColumn(verbose_name='Details',
                                    template_name='taskdatabase/query/action_buttons_per_row.html',
                                    orderable=False)  # orderable not sortable

    buttons = tables.TemplateColumn(verbose_name='Set Status',
                                    template_name='taskdatabase/query/status_buttons_per_row.html',
                                    orderable=False)  # orderable not sortable

