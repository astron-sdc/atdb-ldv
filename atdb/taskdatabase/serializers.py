from rest_framework import serializers
from .models import Status, Task, Activity, Workflow, LogEntry, Configuration, Job, PostProcessingRule, Monitor, LatestMonitor

class ActivitySerializer(serializers.ModelSerializer):

    class Meta:
        model = Activity
        fields = "__all__"

class WorkflowSerializer(serializers.ModelSerializer):

    class Meta:
        model = Workflow
        fields = "__all__"


class LogEntrySerializer(serializers.ModelSerializer):

    class Meta:
        model = LogEntry
        fields = "__all__"


class TaskWriteSerializer(serializers.ModelSerializer):

    status_history = serializers.StringRelatedField(
        many=True,
        required=False,
    )

    log_entries = serializers.StringRelatedField(
        many=True,
        required=False
    )

    successors = serializers.StringRelatedField(
        many=True,
        required=False,
    )

    new_workflow_id = serializers.SerializerMethodField()  # no corresponding model property.
    new_workflow_uri = serializers.SerializerMethodField()  # no corresponding model property.

    class Meta:
        model = Task
        fields = ('id','task_type','filter','predecessor','successors',
                  'joined_output_task',
                  'project','sas_id','priority','purge_policy','cleanup_policy','resume','is_aggregated','is_summary',
                  'new_workflow_id','new_workflow_uri','workflow',
                  'stage_request_id',
                  'status','new_status','quality','calculated_qualities',
                  'inputs','outputs','metrics','status_history','remarks',
                  'size_to_process','size_processed','total_processing_time',
                  'log_entries','meta_scheduling','environment','archive','service_filter'
                  )

    def get_new_workflow_id(self, instance):
        return instance.workflow.id

    def get_new_workflow_uri(self, instance):
        return instance.workflow.workflow_uri

    def create(self, validated_data):
        task = Task.objects.create(**validated_data)

        workflow_uri = self.initial_data['new_workflow_uri']
        if workflow_uri:
            workflow = Workflow.objects.get(workflow_uri=workflow_uri)
            task.workflow = workflow
            task.save()
            return task

        workflow_id = self.initial_data['new_workflow_id']
        if workflow_id:
            workflow = Workflow.objects.get(id=workflow_id)
            task.workflow = workflow
            task.save()
            return task


class TaskReadSerializer(serializers.ModelSerializer):

    status_history = serializers.StringRelatedField(
        many=True,
        required=False,
    )

    log_entries = serializers.StringRelatedField(
        many=True,
        required=False
    )

    successors = serializers.StringRelatedField(
        many=True,
        required=False,
    )

    class Meta:
        model = Task
        fields = ['id','task_type','is_summary','is_aggregated','creationTime','filter',
                  'predecessor','predecessor_status','successors',
                  'joined_input_tasks','joined_output_task','joined_status',
                  'project','sas_id','priority','purge_policy','cleanup_policy','resume',
                  'workflow',
                  'stage_request_id',
                  'status','new_status','quality','calculated_qualities',
                  'inputs','outputs','metrics','remarks','status_history',
                  'size_to_process', 'size_processed', 'total_processing_time','nr_of_dps',
                  'log_entries','meta_scheduling','environment','archive',
                  'activity','service_filter','ingest_location'
                  ]
        read_only_fields = fields


#/atdb/tasks-fast/
class TaskReadSerializerFast(serializers.ModelSerializer):
    """
    status_history = serializers.StringRelatedField(
        many=True,
        required=False,
    )

    log_entries = serializers.StringRelatedField(
        many=True,
        required=False
    )

    successors = serializers.StringRelatedField(
        many=True,
        required=False,
    )
    """
    class Meta:
        model = Task
        fields = ['id','task_type','is_summary','is_aggregated','creationTime','filter','predecessor','predecessor_status',
                  #'joined_input_tasks', 'joined_output_task', 'joined_status',
                  'project','sas_id','priority','purge_policy','cleanup_policy','resume',
                  'workflow',
                  'stage_request_id',
                  'status','new_status','quality','calculated_qualities',
                  'inputs','outputs','metrics','archive',
                  'size_to_process', 'size_processed', 'total_processing_time','nr_of_dps'
                  ]
        read_only_fields = fields


class StatusSerializer(serializers.ModelSerializer):

    class Meta:
        model = Status
        fields = "__all__"


class WorkflowSerializer(serializers.ModelSerializer):

    class Meta:
        model = Workflow
        fields = "__all__"


class ConfigurationSerializer(serializers.ModelSerializer):

    class Meta:
        model = Configuration
        fields = "__all__"


class JobSerializer(serializers.ModelSerializer):

    class Meta:
        model = Job
        fields = "__all__"
        fields = ['id','type','task_id','job_id','timestamp','metadata','webdav_url']

class PostProcessingRuleSerializer(serializers.ModelSerializer):

    class Meta:
        model = PostProcessingRule
        fields = "__all__"


class MonitorSerializer(serializers.ModelSerializer):

    class Meta:
        model = Monitor
        fields = "__all__"


class LatestMonitorSerializer(serializers.ModelSerializer):

    class Meta:
        model = LatestMonitor
        fields = "__all__"
        #read_only_fields = fields