# Generated by Django 3.1.4 on 2023-07-07 09:44

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('taskdatabase', '0029_auto_20230707_1135'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='task',
            name='joined_input_tasks',
        ),
        migrations.AddField(
            model_name='task',
            name='joined_output_task',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='joined_input_tasks', to='taskdatabase.task'),
        ),
    ]
