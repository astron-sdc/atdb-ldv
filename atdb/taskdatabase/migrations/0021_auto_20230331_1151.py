# Generated by Django 3.1.4 on 2023-03-31 09:51

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('taskdatabase', '0020_auto_20230327_1029'),
    ]

    operations = [
        migrations.AddField(
            model_name='workflow',
            name='prefetch',
            field=models.BooleanField(default=True, null=True),
        ),
        migrations.AlterField(
            model_name='logentry',
            name='description',
            field=models.CharField(blank=True, max_length=255, null=True),
        ),
    ]
