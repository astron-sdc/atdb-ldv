# 0051_populate_service_host_name.py

from django.db import migrations, models
import json


def populate_service_host_name(apps, schema_editor):
    Task = apps.get_model('taskdatabase', 'Task')

    # Iterate over all Task instances
    for task in Task.objects.all():
        inputs = task.inputs  # Assuming `inputs` is a JSONField that stores JSON data

        # Check if inputs has the required 'location' key
        try:
            location = inputs[0].get("location")

            # check if any tasks/activities should not use the default 'srm.grid.sara.nl'
            default_location = 'srm.grid.sara.nl'
            if location != default_location:

                # Also set the Activity's service_host_name if there is a related Activity
                print(f'{task.activity} => {location}')

                task.activity.service_filter = location
                task.activity.save()

                task.service_filter = location
                task.save()
        except:
            pass

class Migration(migrations.Migration):
    dependencies = [
        ('taskdatabase', '0050_activity_service_filter_task_service_filter'),  # Replace with the actual name of the previous migration
    ]

    operations = [
        migrations.RunPython(populate_service_host_name),
    ]
