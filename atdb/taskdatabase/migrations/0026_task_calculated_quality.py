# Generated by Django 3.1.4 on 2023-06-20 07:54

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('taskdatabase', '0025_auto_20230509_1631'),
    ]

    operations = [
        migrations.AddField(
            model_name='task',
            name='calculated_quality',
            field=models.CharField(blank=True, max_length=10, null=True),
        ),
    ]
