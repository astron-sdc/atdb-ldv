# Generated by Django 5.0 on 2024-02-09 08:06

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('taskdatabase', '0035_task_activity'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='activity',
            name='output_sas_id',
        ),
        migrations.AddField(
            model_name='activity',
            name='remaining',
            field=models.FloatField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='activity',
            name='total_size',
            field=models.FloatField(blank=True, null=True),
        ),
    ]
