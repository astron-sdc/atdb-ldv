# Generated by Django 3.1.4 on 2022-02-11 07:22

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('taskdatabase', '0012_task_environment'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='task',
            name='environment',
        ),
    ]
