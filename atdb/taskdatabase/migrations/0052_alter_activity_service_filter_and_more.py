# Generated by Django 5.0 on 2024-11-08 08:21

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('taskdatabase', '0051_populate_service_host_name'),
    ]

    operations = [
        migrations.AlterField(
            model_name='activity',
            name='service_filter',
            field=models.CharField(blank=True, max_length=50, null=True),
        ),
        migrations.AlterField(
            model_name='task',
            name='service_filter',
            field=models.CharField(blank=True, max_length=50, null=True),
        ),
    ]
