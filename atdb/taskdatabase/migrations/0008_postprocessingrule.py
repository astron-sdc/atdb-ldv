# Generated by Django 3.1.4 on 2022-01-17 13:50

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('taskdatabase', '0007_auto_20211214_0941'),
    ]

    operations = [
        migrations.CreateModel(
            name='PostProcessingRule',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('aggregation_key', models.CharField(blank=True, db_index=True, default=None, max_length=20, null=True)),
                ('trigger_status', models.CharField(blank=True, db_index=True, default='unknown', max_length=50, null=True)),
                ('workflow_to_apply', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='to_apply', to='taskdatabase.workflow')),
                ('workflow_to_process', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='to_process', to='taskdatabase.workflow')),
            ],
        ),
    ]
