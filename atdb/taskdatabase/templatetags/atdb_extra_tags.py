from django import template

register = template.Library()

@register.filter
def expand_list(list):
    s = ''
    for key, value in list:
        s = s + '&' + key + '=' + value
    return s

@register.filter
def current_query_params(request):
    # expands the current query parameters into a string like:
    # ?id=&status__icontains=defining&sas_id=178677
    params = "?"+request.session['current_query_params'][1:]
    return params