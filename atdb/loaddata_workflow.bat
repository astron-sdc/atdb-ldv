REM load the workflows table from a json file (fixture)

python manage.py loaddata workflow_fixture.json --settings=atdb.settings.dev

REM sdc-dev / sdc
REM docker exec -it atdb-ldv python manage.py loaddata workflow_fixture.json  --indent 4 --settings=atdb.settings.docker_sdc
