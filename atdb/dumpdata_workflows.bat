REM dump the workflows tables as a json file (fixture)

python manage.py dumpdata taskdatabase.workflow --indent 4 --settings=atdb.settings.dev > workflow_fixture.json

REM sdc-dev / sdc
REM docker exec -it atdb-ldv python manage.py dumpdata taskdatabase.workflow  --indent 4 --settings=atdb.settings.docker_sdc > workflow_fixture.json
