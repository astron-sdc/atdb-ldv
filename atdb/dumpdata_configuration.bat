REM dump the configuration table as a json file (fixture)

python manage.py dumpdata taskdatabase.configuration --indent 4 --settings=atdb.settings.dev > configuration_fixture.json

REM sdc-dev / sdc
REM docker exec -it atdb-ldv python manage.py dumpdata taskdatabase.configuration  --indent 4 --settings=atdb.settings.docker_sdc > configuration_fixture.json
