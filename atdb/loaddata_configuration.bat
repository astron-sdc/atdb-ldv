REM load the configuration_fixture.json from a json file

python manage.py loaddata configuration_fixture.json --settings=atdb.settings.dev

REM sdc-dev / sdc
REM docker exec -it atdb-ldv python manage.py loaddata /shared/configuration_fixture.json --settings=atdb.settings.docker_sdc
