#!/usr/bin/bash

export ATDB_DIR=$HOME/my_docker/atdb-ldv

while true; do
    cd $ATDB_DIR
    git pull

    # Proceed with build and redeploy, this only does something when new changes were detected by the git pull
    cd atdb
    docker build -t atdb-ldv:latest .
    docker-compose -f docker/docker-compose-dev-pull-local.yml -p atdb up -d

    sleep 60  # Wait for 1 minute before the next iteration

done